import time
from random import random
from pylsl import StreamInfo, StreamOutlet
srate = 128.0
info = StreamInfo('SimEEG', 'EEG', 32, srate, 'float32')
eegOut = StreamOutlet(info)
n = 10
mrkOut = []
for k in range(n):
    info = StreamInfo('SimMarker'+str(k), 'Markers', 1, 0, 'float32')
    mrkOut.append(StreamOutlet(info))
while True:
    t0 = time.time()
    eegOut.push_sample([random()]*32);
    for k,mrkOut_k in enumerate(mrkOut):
        mrkOut_k.push_sample([random()]);
    while time.time()-t0 < 1.0/srate:
        pass
