import time
import numpy as np
from scipy.io.matlab import savemat
from pylsl import ContinuousResolver, StreamInlet

resolver = ContinuousResolver(prop='type', value='EEG');
while len(resolver.results()) == 0:
    time.sleep(0.25)
eegIn = []
for info in resolver.results():
    eegIn.append(StreamInlet(info))

resolver = ContinuousResolver(prop='type', value='Markers');
while len(resolver.results()) == 0:
    time.sleep(0.25)
mrkIn = []
for info in resolver.results():
    mrkIn.append(StreamInlet(info))
n_eeg = len(eegIn)
n_mrk = len(mrkIn)
latency = []
tsample = [0]*(n_eeg+n_mrk)
T0 = time.time()
while time.time()-T0 < 75:
    for k,eegIn_k in enumerate(eegIn):
        s, tsample[k] = eegIn_k.pull_sample();
    for k,mrkIn_k in enumerate(mrkIn):
        m, tsample[k+n_eeg] = mrkIn_k.pull_sample();
    #print(tsample)
    latency.append(tsample)
savemat('timeDelay_pylsl.mat',{'timeDelay':np.array(latency)})
