# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'preferences.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Preferences_Dialog(QtWidgets.QDialog):
    def __init__(self, parent):
        self.parent = parent
        super(Preferences_Dialog, self).__init__()
        self.setupUi()
        self.show()

    def setupUi(self):
        self.setObjectName("Dialog")
        self.resize(218, 116)
        self.gridLayout = QtWidgets.QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.radioButtonLSL = QtWidgets.QRadioButton(self)
        self.radioButtonLSL.setChecked(True)
        self.radioButtonLSL.setObjectName("radioButton")
        self.radioButtonLSL.toggled.connect(lambda: self.onChange(self.radioButtonLSL))
        self.horizontalLayout.addWidget(self.radioButtonLSL)
        self.radioButtonTCP = QtWidgets.QRadioButton(self)
        self.radioButtonTCP.setObjectName("radioButton_2")
        self.radioButtonTCP.toggled.connect(lambda: self.onChange(self.radioButtonTCP))
        self.horizontalLayout.addWidget(self.radioButtonTCP)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 2)
        self.label = QtWidgets.QLabel(self)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self)
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.setText(self.parent.streamName)
        self.gridLayout.addWidget(self.lineEdit, 1, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)
        self.buttonBox.raise_()
        self.radioButtonLSL.raise_()
        self.radioButtonLSL.raise_()
        self.label.raise_()
        self.lineEdit.raise_()

        self.retranslateUi()
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Dialog", "Preferences"))
        self.radioButtonLSL.setText(_translate("Dialog", "Use LSL"))
        self.radioButtonTCP.setText(_translate("Dialog", "Use TCP"))
        self.label.setText(_translate("Dialog", "Stream name"))

    def onChange(self, btn):
        if btn.text() == "Use LSL":
            if btn.isChecked():
                self.label.setText("Stream name")
                self.lineEdit.setText(self.parent.streamName)
            else:
                self.label.setText("IP:Port")
        if btn.text() == "Use TCP":
            if btn.isChecked():
                self.label.setText("IP:Port")
                self.lineEdit.setText(self.parent.ip+":"+str(self.parent.port))
            else:
                self.label.setText("Stream name")

    def accept(self):
        if self.radioButtonLSL.isChecked():
            self.parent.streamName = self.lineEdit.text()
            self.parent.disconnect()
            self.parent.connect("LSL")
        else:
            ind = self.lineEdit.text().find(":")
            self.parent.ip = self.lineEdit.text()[0:ind]
            self.parent.port = int(self.lineEdit.text()[ind+1:])
            self.parent.connect("TCP")
        self.close()
