import numpy as np
import socket
import struct
from multiprocessing import Process, Queue
import pyqtgraph as pg
from pylsl import StreamInlet, resolve_byprop


def dataReaderLSL(streamName, q):
    while True:
        print("Waiting for LSL stream")
        try:
            results = resolve_byprop(prop='name', value=streamName)
            while len(results) == 0:
                time.sleep(0.25)
            info = results[0]
            inlet = StreamInlet(info, recover=False)
            print("Streaming...")
            # Read data in forever
            try:
                while True:
                    data = inlet.pull_sample()
                    if data:
                        q.put(np.array(data[0]))
                    #time.sleep(1/512)
            except Exception as e:
                print(e)
                pass
        except Exception as e:
            print(e)
            pass


def dataReaderTCP(ip, port, n, q):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, port)
    sock.bind(server_address)
    sock.listen(1)
    print("Waiting for a TCP connection")
    connection, client_address = sock.accept()
    print("Streaming...")
    unpacker = struct.Struct("!"+str(n) + "d")
    try:
        while True:
            try:
                print("Waiting for sample")
                print(unpacker.size)
                data = connection.recv(unpacker.size)
                values = unpacker.unpack(data)
                q.put(values)
                print(q.qsize())
            except Exception as e:
                print(e)
                pass
    finally:
        connection.close()


def human():
    color = [[0,      0,      1,      1],
             [0,      0.8648, 1,      1],
             [0.1348, 0.9093, 0.9430, 1],
             [1,      0.7500, 0.6500, 1],
             [0.943,  0.9093, 0.1348, 1],
             [1.0,    0.8648, 0,      1],
             [1.0,    0,      0,      1]]
    color = np.array(color)
    pos = np.linspace(-1, 1, color.shape[0])
    return pg.ColorMap(pos=pos, color=color, mode="RBG")
