# /usr/bin/python3

# Form implementation generated from reading ui file 'CortexViewer.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

import os
import sys
import time
import numpy as np
import pickle
from collections import deque
from multiprocessing import Process, Queue
import webbrowser

from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import pyqtgraph.opengl as gl
from pyqtgraph.graphicsItems.GradientEditorItem import Gradients
from pyqtgraph.opengl import GLViewWidget

from preferences import Preferences_Dialog
from utils import dataReaderLSL, dataReaderTCP, human


class CortexViewer(object):

    def __init__(self, MainWindow, streamName="cortexViewer", ip="127.0.0.1", port=26002):
        self.setupUi(MainWindow)
        simbsiFolder = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]
        action0 = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","Gnome-zoom-fit-best.svg")), "Autoscale", MainWindow)
        action0.triggered.connect(self.setAutoscale)
        self.toolBar.addAction(action0)
        actionPlus = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","Gnome-list-add.svg")), 'Scale up', MainWindow)
        actionPlus.triggered.connect(self.onScaleUp)
        self.toolBar.addAction(actionPlus)
        actionMinus = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","Gnome-list-remove.svg")), 'Scale down', MainWindow)
        actionMinus.triggered.connect(self.onScaleDown)
        self.toolBar.addAction(actionMinus)
        action1 = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","LeftRightView.png")), 'View L/R hemisphere', MainWindow)
        action1.triggered.connect(self.viewHemisphere)
        self.toolBar.addAction(action1)
        action3 = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","preferences.png")), 'Preferences', MainWindow)
        action3.triggered.connect(self.setPreferences)
        self.toolBar.addAction(action3)
        actionHelp = QtGui.QAction(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","Gnome-help-browser.svg.png")), 'Help', MainWindow)
        actionHelp.triggered.connect(self.help)
        self.toolBar.addAction(actionHelp)
        self.graphicsViewCortex.mouseReleaseEvent = self.mousePressEvent
        self.autoscale = True
        self.scale = 1
        self.cortexViewState = deque([1, 2, 3])
        self.streamName = streamName
        self.ip = ip
        self.port = port
        self.queue = Queue()
        self.dataReaderProcess = None
        self.cortexL = None
        self.cortexR = None
        self.container = MainWindow
        self.colormapCortex = human()
        self.connect()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(450, 390)
        simbsiFolder = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]
        MainWindow.setWindowIcon(QtGui.QIcon(os.path.join(simbsiFolder,"resources","icons","logo.jpg")))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 0, 1, 2)
        self.graphicsViewCortex = GLViewWidget(self.centralwidget)
        self.graphicsViewCortex.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.graphicsViewCortex.setObjectName("graphicsViewCortex")
        self.gridLayout.addWidget(self.graphicsViewCortex, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(MainWindow)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Primary current density view"))

    def createView(self, numberOfSources):
        skinColor = [1, .75, .65, 1]
        if numberOfSources==5003:
            hmfile = os.path.join(os.path.split(os.path.abspath(__file__))[0],"head_model_template_5003.pk")
        else:
            hmfile = os.path.join(os.path.split(os.path.abspath(__file__))[0],"head_model_template_v1.pk")
        with open(hmfile,'br') as f:
            hm = pickle.load(f)
        self.indCortexL = hm['indL'][:, 0] - 1
        self.indCortexR = hm['indR'][:, 0] - 1

        self.nx = hm['cortex']['vertices'].shape[0]
        cortexColor = np.dot(np.ones((self.nx, 1)), np.array([skinColor]))
        center = np.array([0, 0, np.mean(hm['cortex']['vertices'][:, 2])])

        self.cortexL = gl.GLMeshItem(vertexes=hm['cortexL']['vertices']-center,
                                     faces=hm['cortexL']['faces'] - 1,
                                     smooth=True,
                                     vertexColors=cortexColor[self.indCortexL, :],
                                     shader='edgeHilight',
                                     glOptions='opaque',
                                     computeNormals=True,
                                     drawEdges=False)
        self.cortexR = gl.GLMeshItem(vertexes=hm['cortexR']['vertices']-center,
                                     faces=hm['cortexR']['faces'] - 1,
                                     smooth=True,
                                     vertexColors=cortexColor[self.indCortexR, :],
                                     shader='edgeHilight',
                                     glOptions='opaque',
                                     computeNormals=True,
                                     drawEdges=False)
        self.graphicsViewCortex.addItem(self.cortexL)
        self.graphicsViewCortex.addItem(self.cortexR)
        bc = 240
        self.graphicsViewCortex.setBackgroundColor(QtGui.QColor(bc, bc, bc))
        self.graphicsViewCortex.setCameraPosition(distance=0.45, azimuth=35, elevation=0)
        self.graphicsViewCortex.keyPressEvent = self.onKeyPressed

    def help(self):
        webbrowser.open("https://bitbucket.org/neatlabs/simbsi/wiki/Cortex%20Viewer%20Widget")

    def onKeyPressed(self, event):
        print(event.key())
        if event.key() == QtCore.Qt.Key_Plus:
            self.scale *= 2
        elif event.key() == QtCore.Qt.Key_Minus:
            self.scale *= 0.25

    def onScaleUp(self):
        self.scale *= 2

    def onScaleDown(self):
        self.scale *= 0.25

    def setAutoscale(self):
        self.autoscale = not self.autoscale

    def viewHemisphere(self):
        self.cortexViewState.rotate(-1)
        if self.cortexViewState[0] == 1:
            self.cortexL.setVisible(True)
            self.cortexR.setVisible(True)
        elif self.cortexViewState[0] == 2:
            self.cortexL.setVisible(True)
            self.cortexR.setVisible(False)
        elif self.cortexViewState[0] == 3:
            self.cortexL.setVisible(False)
            self.cortexR.setVisible(True)

    def setPreferences(self):
        Preferences_Dialog(self)

    def mousePressEvent(self, event):
        if event.button() == 2:
            # print(self.graphicsViewCortex.mapFromGlobal(event.pos()))
            x, y = event.pos().x(), event.pos().y()
            # print(event.globalPos())
            x = x - self.graphicsViewCortex.width() / 2
            y = y - self.graphicsViewCortex.height() / 2
            # print((x, y))
            # print(self.graphicsViewCortex.cameraPosition())

    def connect(self, connectionType="LSL"):
        if connectionType == "LSL":
            self.dataReaderProcess = Process(target=dataReaderLSL, args=(self.streamName, self.queue))
        else:
            self.dataReaderProcess = Process(target=dataReaderTCP, args=(self.ip, self.port, self.ny + self.nx, self.queue))
        self.graphicsViewCortex.timerEvent = self.update
        self.graphicsViewCortex.startTimer(1000/512)
        self.dataReaderProcess.daemon = True
        self.dataReaderProcess.start()

    def update(self, event):
        if self.queue.qsize():
            value = 0
            self.statusbar.showMessage("Buffering: " + str(self.queue.qsize()))
            for k in range(self.queue.qsize()):
                value += self.queue.get()
            value /= k+1
            if self.autoscale:
                value /= np.max(np.abs(value))+np.finfo(float).eps
            else:
                value *= self.scale
            if self.cortexL is None:
                self.createView(value.shape[0])
            if self.cortexViewState[0] == 1:
                self.cortexL.colors = self.colormapCortex.mapToFloat(value[self.indCortexL])
                self.cortexR.colors = self.colormapCortex.mapToFloat(value[self.indCortexR])
                self.cortexL.update()
                self.cortexR.update()
            elif self.cortexViewState[0] == 2:
                self.cortexL.colors = self.colormapCortex.mapToFloat(value[self.indCortexL])
                self.cortexL.update()
            elif self.cortexViewState[0] == 3:
                self.cortexR.colors = self.colormapCortex.mapToFloat(value[self.indCortexR])
                self.cortexR.update()

    def disconnect(self):
        try:
            self.dataReaderProcess.terminate()
        except Exception as e:
            print(e)

    def __del__(self):
        try:
            self.dataReaderProcess.terminate()
            print("Data reader process terminated.")
        except Exception as e:
            print(e)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        streamName = argv[1]
    except:
        streamName = 'cortexViewer'
    appQt = QtWidgets.QApplication(sys.argv)
    win = QtWidgets.QMainWindow()
    viewer = CortexViewer(win, streamName)
    win.show()
    try:
        appQt.exec_()
    finally:
        del viewer


if __name__ == "__main__":
    sys.exit(main())
