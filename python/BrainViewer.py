# /usr/bin/python3

# Form implementation generated from reading ui file 'BrainViewer.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

import sys
import time
import numpy as np
import pickle
from collections import deque
from multiprocessing import Process, Queue

from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import pyqtgraph.opengl as gl
from pyqtgraph.graphicsItems.GradientEditorItem import Gradients
from pyqtgraph.opengl import GLViewWidget

from preferences import Preferences_Dialog
from utils import dataReaderLSL, dataReaderTCP, human


class BrainViewer(object):

    def __init__(self, MainWindow, streamName="brainViewer", ip="127.0.0.1", port=26001):
        self.setupUi(MainWindow)
        action1 = QtGui.QAction(QtGui.QIcon("../icons/lr_brain.png"), 'View L/R hemisphere', MainWindow)
        action1.triggered.connect(self.viewHemisphere)
        self.toolBar.addAction(action1)
        action2 = QtGui.QAction(QtGui.QIcon("../icons/sensorsOff.png"), 'View sensors', MainWindow)
        action2.triggered.connect(self.viewSensors)
        self.toolBar.addAction(action2)
        action3 = QtGui.QAction(QtGui.QIcon("../icons/preferences.png"), 'View sensors', MainWindow)
        action3.triggered.connect(self.setPreferences)
        self.toolBar.addAction(action3)
        self.graphicsViewCortex.mouseReleaseEvent = self.mousePressEvent

        self.cortexViewState = deque([1, 2, 3])
        self.streamName = streamName
        self.ip = ip
        self.port = port
        self.queue = Queue()
        self.dataReaderProcess = None
        skinColor = [1, .75, .65, 1]
        with open("head_model_template.pk",'br') as f:
            hm = pickle.load(f)
        # hm = loadmat('../resources/minimal_head_modelColin27_5003_Standard-10-5-Cap33.mat')
        self.indCortexL = np.array(hm['indL'])[:, 0] - 1
        self.indCortexR = np.array(hm['indR'])[:, 0] - 1

        self.ny = np.array(hm['scalp'][0][0][0]).shape[0]
        self.nx = np.array(hm['cortex'][0][0][0]).shape[0]
        scalpColor = np.dot(np.ones((self.ny, 1)), np.array([skinColor]))
        cortexColor = np.dot(np.ones((self.nx, 1)), np.array([skinColor]))

        vcortex = np.array(hm['cortex'][0][0][0])
        center = np.array([0, 0, np.mean(vcortex[:, 2])])
        self.scalp = gl.GLMeshItem(vertexes=np.array(hm['scalp'][0][0][0]),
                                   faces=np.array(hm['scalp'][0][0][1])-1,
                                   smooth=True,
                                   vertexColors=scalpColor,
                                   shader='edgeHilight',
                                   glOptions='opaque',
                                   drawEdges=False)
        self.cortexL = gl.GLMeshItem(vertexes=np.array(hm['cortexL'][0][0][0])-center,
                                     faces=np.array(hm['cortexL'][0][0][1]) - 1,
                                     smooth=True,
                                     vertexColors=cortexColor[self.indCortexL, :],
                                     shader='edgeHilight',
                                     glOptions='opaque',
                                     computeNormals=True,
                                     drawEdges=False)
        self.cortexR = gl.GLMeshItem(vertexes=np.array(hm['cortexR'][0][0][0])-center,
                                     faces=np.array(hm['cortexR'][0][0][1]) - 1,
                                     smooth=True,
                                     vertexColors=cortexColor[self.indCortexR, :],
                                     shader='edgeHilight',
                                     glOptions='opaque',
                                     computeNormals=True,
                                     drawEdges=False)
        self.sensors = gl.GLScatterPlotItem(pos=hm['sensors'], pxMode=True)
        self.indz = np.array(hm['scalp'][0][0][0]) < 0.98*np.min(hm['sensors'][:, 2])
        self.sensors.setVisible(False)
        self.graphicsViewScalp.addItem(self.scalp)
        self.graphicsViewScalp.addItem(self.sensors)
        self.graphicsViewCortex.addItem(self.cortexL)
        self.graphicsViewCortex.addItem(self.cortexR)
        for gv in [self.graphicsViewScalp, self.graphicsViewCortex]:
            gv.setBackgroundColor("w")
            gv.setCameraPosition(distance=0.35, azimuth=35, elevation=0)
            gv.timerEvent = self.update
        self.container = MainWindow
        self.graphicsViewScalp.startTimer(1000/512)
        self.colormapScalp = human()
        self.colormapCortex = human()
        self.connect()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(550, 390)
        MainWindow.setWindowIcon(QtGui.QIcon("../icons/logo.jpg"))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 0, 1, 2)
        self.graphicsViewCortex = GLViewWidget(self.centralwidget)
        self.graphicsViewCortex.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.graphicsViewCortex.setObjectName("graphicsViewCortex")
        self.gridLayout.addWidget(self.graphicsViewCortex, 1, 1, 1, 1)
        self.graphicsViewScalp = GLViewWidget(self.centralwidget)
        self.graphicsViewScalp.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.graphicsViewScalp.setObjectName("graphicsViewScalp")
        self.gridLayout.addWidget(self.graphicsViewScalp, 1, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(MainWindow)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "BrainViewer"))
        self.label.setText(_translate("MainWindow", "EEG topographic view"))
        self.label_2.setText(_translate("MainWindow", "Primary current density view"))

    def viewHemisphere(self):
        self.cortexViewState.rotate(-1)
        if self.cortexViewState[0] == 1:
            self.cortexL.setVisible(True)
            self.cortexR.setVisible(True)
        elif self.cortexViewState[0] == 2:
            self.cortexL.setVisible(True)
            self.cortexR.setVisible(False)
        elif self.cortexViewState[0] == 3:
            self.cortexL.setVisible(False)
            self.cortexR.setVisible(True)

    def viewSensors(self):
        self.sensors.setVisible(not self.sensors.visible())

    def setPreferences(self):
        Preferences_Dialog(self)

    def mousePressEvent(self, event):
        if event.button() == 2:
            # print(self.graphicsViewCortex.mapFromGlobal(event.pos()))
            x, y = event.pos().x(), event.pos().y()
            # print(event.globalPos())
            x = x - self.graphicsViewCortex.width() / 2
            y = y - self.graphicsViewCortex.height() / 2
            # print((x, y))
            # print(self.graphicsViewCortex.cameraPosition())

    def connect(self, connectionType="LSL"):
        self.disconnect()
        if connectionType == "LSL":
            self.dataReaderProcess = Process(target=dataReaderLSL, args=(self.streamName, self.queue))
        else:
            self.dataReaderProcess = Process(target=dataReaderTCP, args=(self.ip, self.port, self.ny + self.nx, self.queue))
        self.dataReaderProcess.daemon = True
        self.dataReaderProcess.start()

    def update(self, event):
        if self.queue.qsize():
            value = 0
            self.statusbar.showMessage("Buffering: " + str(self.queue.qsize()))
            for k in range(self.queue.qsize()):
                value += self.queue.get()
            value /= k+1
            y = value[0:self.ny]
            self.scalp.colors = self.colormapScalp.mapToFloat(y)
            if len(value) == self.ny+self.nx:
                x = value[self.ny:]
                if self.cortexViewState[0] == 1:
                    self.cortexL.colors = self.colormapCortex.mapToFloat(x[self.indCortexL])
                    self.cortexR.colors = self.colormapCortex.mapToFloat(x[self.indCortexR])
                    self.cortexL.update()
                    self.cortexR.update()
                elif self.cortexViewState[0] == 2:
                    self.cortexL.colors = self.colormapCortex.mapToFloat(x[self.indCortexL])
                    self.cortexL.update()
                elif self.cortexViewState[0] == 3:
                    self.cortexR.colors = self.colormapCortex.mapToFloat(x[self.indCortexR])
                    self.cortexR.update()
            self.scalp.update()

    def disconnect(self):
        try:
            self.dataReaderProcess.terminate()
        except Exception as e:
            print(e)

    def __del__(self):
        try:
            self.dataReaderProcess.terminate()
            print("Data reader process terminated.")
        except Exception as e:
            print(e)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        streamName = argv[1]
    except:
        streamName = 'brainViewer'
    appQt = QtWidgets.QApplication(sys.argv)
    win = QtWidgets.QMainWindow()
    viewer = BrainViewer(win, streamName)
    win.show()
    try:
        appQt.exec_()
    finally:
        del viewer


if __name__ == "__main__":
    sys.exit(main())
