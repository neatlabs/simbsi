from pynput import keyboard
from pylsl import StreamInfo, StreamOutlet

info = StreamInfo('KeyLogger', 'Markers', 1, 0, 'float32')
outlet = StreamOutlet(info)
value = None

def on_press(key):
    try:
        value = ord(key.char)
    except:
        try:
            value = key.value.vk
        except:
            value = -1
    print(value)
    outlet.push_sample([value]);

def on_release(key):
    outlet.push_sample([0.01]);
    #outlet.push_sample([0]);
    if key == keyboard.Key.esc:
        # Stop listener
        return False

# Collect events until released
with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()
