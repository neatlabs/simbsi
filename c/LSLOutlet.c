/*
 * 
 * Abstract:
 *       C-file S-function for sending data to the Lab Streaming Layer (LSL). 
 *	 
 * This file is part of the SimBSI project, see more at https://bitbucket.org/neatlabs/simbsi/wiki/Home.
 *
 * This code is provided onder a BSD type license (educational, research and non-profit purposes). See 
 * other terms and conditions in https://bitbucket.org/neatlabs/simbsi/src/master/license.txt.
 *
 * Author: Alejandro Ojeda, 2018, Neural Engineering and Translation Labs, University of California San Diego
 */


#define S_FUNCTION_NAME  LSLOutlet
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lsl_c.h"
#include "simbsi.h"


/* Function: getID =============================================== 
 * Abstract: 
 *   Generates random source id.
 */
void getID(const int maxLength, char *id)
{
    static const char alphanum[] = "abcdefghijklmnopqrstuvwxyz0123456789";
    for(int i=0; i<maxLength; i++)
        id[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    id[maxLength-1] = 0;
}   

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *   Setup sizes of the various vectors.
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 6);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }
    //ssSetSFcnParamTunable(S,0,false);
    //ssSetSFcnParamTunable(S,1,false);
    //ssSetSFcnParamTunable(S,2,false);
    //ssSetSFcnParamTunable(S,3,false);
    //ssSetSFcnParamTunable(S,4,false);
    //ssSetSFcnParamTunable(S,5,false);
    
    char *streamName;
    char *streamType;
    char *channelLabels;
    int idLength = 32;
    char sourceID[32];
    getID(idLength, sourceID);
    int numberOfChannels;
    int chunkSize;
    float samplingRate;
    char *label;
    int_T buflen;
    int_T status;
    lsl_xml_ptr desc, chn, chns;/* some xml element pointers */ 
    
    // Parse input parameters 
    buflen = mxGetN(ssGetSFcnParam(S, 0))*sizeof(mxChar)+1;
    streamName = calloc(mxGetN(ssGetSFcnParam(S, 0)), sizeof(mxChar));
    status = mxGetString((ssGetSFcnParam(S, 0)), streamName,buflen);
    mexPrintf("Stream name: %s\n", streamName);
    
    buflen = mxGetN(ssGetSFcnParam(S, 1))*sizeof(mxChar)+1;
    streamType = calloc(mxGetN((ssGetSFcnParam(S, 1))), sizeof(mxChar));
    status = mxGetString((ssGetSFcnParam(S, 1)), streamType,buflen);
    mexPrintf("Stream type: %s\n", streamType);
    
    numberOfChannels = *mxGetPr(ssGetSFcnParam(S, 2));
    mexPrintf("Number of channels: %d\n", numberOfChannels);
    
    chunkSize = *mxGetPr(ssGetSFcnParam(S, 3));
    mexPrintf("Chunk size: %d\n", chunkSize);
    
    buflen = mxGetN(ssGetSFcnParam(S, 4))*sizeof(mxChar)+1;
    channelLabels = calloc(mxGetN(ssGetSFcnParam(S, 4)), sizeof(mxChar));
    status = mxGetString((ssGetSFcnParam(S, 4)), channelLabels,buflen);
       
    samplingRate = *mxGetPr(ssGetSFcnParam(S, 5));
    mexPrintf("Sampling rate: %f\n", samplingRate);
    mexPrintf("Source ID: %s\n", sourceID);
    
    // Configure ports
    if (!ssSetNumInputPorts(S, 2)) return;
    ssSetInputPortWidth(S, 0, numberOfChannels*chunkSize);
    ssSetInputPortDataType(S, 0, SS_DOUBLE);
    ssSetInputPortComplexSignal(S, 0, COMPLEX_NO);
    ssSetInputPortOptimOpts(S, 0, SS_NOT_REUSABLE_AND_LOCAL);
    ssSetInputPortDirectFeedThrough(S, 0, 1);
    
    ssSetInputPortWidth(S, 1, chunkSize);
    ssSetInputPortDataType(S, 1, SS_DOUBLE);
    ssSetInputPortComplexSignal(S, 1, COMPLEX_NO);
    ssSetInputPortOptimOpts(S, 1, SS_NOT_REUSABLE_AND_LOCAL);
    ssSetInputPortDirectFeedThrough(S, 1, 1);
    if (!ssSetNumOutputPorts(S,0)) return;

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_WORKS_WITH_CODE_REUSE));
    
    /* Declare a streaminfo */
    lsl_streaminfo info = lsl_create_streaminfo(
            streamName, 
            streamType,
            numberOfChannels,
            samplingRate,
            cft_float32,
            sourceID);
    
    // Add stream's metadata
    desc = lsl_get_desc(info);
	chns = lsl_append_child(desc,"channels");
    chn = lsl_append_child(chns,"channel");
    if (strlen(channelLabels)){
        label = strtok (channelLabels," ,.-'");
        lsl_append_child_value(chn,"label",label);
        lsl_append_child_value(chn,"unit","microvolts");
        lsl_append_child_value(chn,"type",streamType);
        mexPrintf("Labels: ");
        for (int c=1;c<numberOfChannels;c++) {
            chn = lsl_append_child(chns,"channel");
            label = strtok (NULL," ,.-'");
            lsl_append_child_value(chn,"label",label);
            mexPrintf("%s ",label);
            lsl_append_child_value(chn,"unit","microvolts");
            lsl_append_child_value(chn,"type",streamType);
        }
        mexPrintf("\n");
    }
    
    OutletUserData* userData = calloc(sizeof(OutletUserData),1);
    if (userData == NULL){
        ssSetErrorStatus(S,"ERROR: Out of memmory");
        return;
    }
    userData->name = streamName;
    userData->chunkSize = chunkSize;
    userData->samplingRate = samplingRate;
    userData->numberOfChannels = numberOfChannels;
    userData->sample = calloc(sizeof(double),userData->numberOfChannels);
    userData->DATA = calloc(sizeof(double),numberOfChannels*userData->chunkSize);
    userData->data = calloc(sizeof(double *),numberOfChannels);
    for (int i=0; i<numberOfChannels; i++)
  		userData->data[i] = userData->DATA + (i * userData->chunkSize);
    userData->outlet = lsl_create_outlet(info,0,360);
    mexPrintf("Outlet %s created\n", streamName);
    ssSetUserData(S,userData);
    lsl_push_sample_d(userData->outlet, userData->DATA);
    free(streamType);
    free(channelLabels);    
    lsl_destroy_streaminfo(info);
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetSampleTime(S, 1, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetOffsetTime(S, 1, 0.0);
}


/* Function: mdlOutputs =======================================================
 * Abstract:
 *    Send data sample or chunk to LSL.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    OutletUserData* userData = (OutletUserData*)ssGetUserData(S);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
    InputRealPtrsType tPtrs = ssGetInputPortRealSignalPtrs(S,1);
    int errcode = lsl_no_error;
    
    if (lsl_have_consumers(userData->outlet)==0)
        return;
    
    for (int i=0; i<userData->numberOfChannels*userData->chunkSize; i++)
        userData->DATA[i] = *uPtrs[i];
    
    for (int i=0; i<userData->chunkSize; i++){
        
        // Copy sample
        for (int ch=0; ch<userData->numberOfChannels; ch++)
            userData->sample[ch] = userData->data[ch][i];
        
        // Push sample
        if (*tPtrs==0)
            errcode = lsl_push_sample_dt(userData->outlet, userData->sample, *tPtrs[i]);
        else
            errcode = lsl_push_sample_d (userData->outlet, userData->sample);
        
        // Check for errors
        if (errcode){
            static char msg[255];
            if (*tPtrs[0]==0)
                sprintf(msg,"Error calling function lsl_push_sample_dt. Error code %d.",errcode);
            else
                sprintf(msg,"Error calling function lsl_push_sample_d. Error code %d.",errcode);
            ssSetErrorStatus(S,msg);
        }
    }
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    Release resources.
 */
static void mdlTerminate(SimStruct *S)
{   
    OutletUserData* userData = (OutletUserData*)ssGetUserData(S);
    lsl_destroy_outlet(userData->outlet);
    free(userData->sample);
    free(userData->DATA);
    free(userData->data);
    free(userData->name);
    free(userData);
    mexPrintf("Outlet %s destroyed\n", userData->name);
}


//#if defined(MATLAB_MEX_FILE)
//#define MDL_RTW
/* Function: mdlRTW ===========================================================
 * Abstract:
 *	Since we've declared all are parameters as non-tunable, we need
 *	only provide this routine so that they aren't written to the model.rtw
 *	file. The values of the parameters are implicitly encoded in the
 *	sample times.
 */
//static void mdlRTW(SimStruct *S)
//{
//}
//#endif /* MDL_RTW */


#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
