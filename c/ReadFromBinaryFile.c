/*
 * 
 * Abstract:
 *       C-file S-function for sending simulating an EEG streaming device. The data samples and events
 *       are pulled from an EEGLAB compatible file. EEGLAB and relevant import functions are need. 
 *	 
 * This file is part of the SimBSI project, see more at https://bitbucket.org/neatlabs/simbsi/wiki/Home.
 *
 * This code is provided onder a BSD type license (educational, research and non-profit purposes). See 
 * other terms and conditions in https://bitbucket.org/neatlabs/simbsi/src/master/license.txt.
 *
 * Author: Alejandro Ojeda, 2018, Neural and Translation Labs, University of California San Diego
 */


#define S_FUNCTION_NAME  ReadFromBinaryFile
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct UserData{
    float samplingRate;
    int numberOfSamples;
    int numberOfChannels;
    float* events;
    float* DATA;
    float** data;
    int pointer;
    clock_t time;
}UserData;


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *   Setup sizes of the various vectors.
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 1);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    int_T buflen = mxGetN(ssGetSFcnParam(S, 0))*sizeof(mxChar)+1;
    char *filename = calloc(mxGetN(ssGetSFcnParam(S, 0)), sizeof(mxChar));
    if (filename == NULL){
        ssSetErrorStatus(S,"ERROR: Out of memmory");
        return;
    }
    int_T status = mxGetString((ssGetSFcnParam(S, 0)), filename,buflen);
    char* ptr;
    ptr = strstr(filename,".set");
    if(ptr==NULL){
        ptr = strstr(filename,".bdf");
        if(ptr==NULL){
            ptr = strstr(filename,".xdf");
            if(ptr==NULL)
                ssSetErrorStatus(S,"This block can read .set, .bdf, or .xdf files only.");    
        }
    }
    strncpy(ptr,".bin",4);
    FILE* fid = fopen(filename,"rb");
    if (fid == NULL){
        ssSetErrorStatus(S,"File not found.");
        return;
    }
    float samplingRate;
    int numberOfSamples;
    int numberOfChannels;
    fread(&samplingRate, sizeof(float), 1, fid);
    fread(&numberOfSamples, sizeof(int), 1, fid);
    fread(&numberOfChannels, sizeof(int), 1, fid);

    // Set ports
    if (!ssSetNumInputPorts(S, 0)) return;
    if (!ssSetNumOutputPorts(S,2)) return;
    ssSetOutputPortVectorDimension(S, 0, numberOfChannels-1);
    ssSetOutputPortDataType(S, 0, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 0, COMPLEX_NO);
    ssSetOutputPortOptimOpts(S, 0, SS_NOT_REUSABLE_AND_LOCAL);
    
    ssSetOutputPortWidth(S, 1, 1);
    ssSetOutputPortDataType(S, 1, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 1, COMPLEX_NO);
    ssSetOutputPortOptimOpts(S, 1, SS_NOT_REUSABLE_AND_LOCAL);

    // Set other Simulink parameters
    ssSetNumSampleTimes(S, -1);
        
    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_WORKS_WITH_CODE_REUSE));
    
    UserData* userData = calloc(sizeof(UserData),1);
    if (userData == NULL){
        ssSetErrorStatus(S,"ERROR: Out of memmory");
        return;
    }
    userData->samplingRate = samplingRate;
    userData->numberOfSamples = numberOfSamples;
    userData->numberOfChannels = numberOfChannels;
    
    userData->events = (float*)calloc( sizeof(float),numberOfSamples);
    userData->DATA = calloc(sizeof(float),numberOfSamples*(numberOfChannels-1));
    userData->data = calloc(sizeof(float *),numberOfChannels-1);
    for (int i=0; i<(numberOfChannels-1); i++)
  		userData->data[i] = userData->DATA + (i * numberOfSamples);
    fread(userData->DATA, sizeof(float),numberOfSamples*(numberOfChannels-1), fid);
    fread(userData->events, sizeof(float), numberOfSamples, fid);
    
    userData->pointer = 0;
    userData->time = clock();
    ssSetUserData(S,userData);
    fclose(fid);
    free(filename);
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    UserData* userData = (UserData*)ssGetUserData(S);
    ssSetSampleTime(S, 0, 1.0/userData->samplingRate);
    ssSetSampleTime(S, 1, 1.0/userData->samplingRate);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetOffsetTime(S, 1, 0.0);
}


/* Function: mdlOutputs =======================================================
 * Abstract:
 *    Output a data sample and event marker. If there is no event marker at this 
 *    time point the second output is 0, otherwise it is a numeric code. When the
 *    sample counter reaches the end of the file it resets to the first sample and 
 *    continue streaming.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    UserData* userData = (UserData*)ssGetUserData(S);
    clock_t t;
    double timeElapsed;
    do{
        t = clock();
        timeElapsed = ((double)(t - userData->time))/CLOCKS_PER_SEC; // in seconds
    }while(timeElapsed < 1.0/userData->samplingRate);
    userData->time = t;
    userData->pointer += 1;
    if (userData->pointer==userData->numberOfSamples)
        userData->pointer = 0;
    real_T *sample = ssGetOutputPortRealSignal(S,0);
    real_T *event = ssGetOutputPortRealSignal(S,1);
    for (int i=0; i<userData->numberOfChannels-1; i++)
        sample[i] = userData->data[i][userData->pointer];
    event[0] = userData->events[userData->pointer];
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    Release resources.
 */
static void mdlTerminate(SimStruct *S)
{
    UserData* userData = (UserData*)ssGetUserData(S);
    free(userData->events);
    free(userData->data);
    free(userData->DATA);
    free(userData);
    mexPrintf("Buffer destroyed\n");
}


#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
