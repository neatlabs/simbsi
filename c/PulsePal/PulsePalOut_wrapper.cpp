
/*
 * Include Files
 *
 */
#include "simstruc.h"



/* %%%-SFUNWIZ_wrapper_includes_Changes_BEGIN --- EDIT HERE TO _END */
#include <math.h>
#include <vector>
#include <stdio.h>
#include <stdint.h>
#include "stdafx.h"
#include "PulsePal.h"
#include "PulsePal.cpp"
#include "ofSerial.h"
#include "ofSerial.cpp"
/* %%%-SFUNWIZ_wrapper_includes_Changes_END --- EDIT HERE TO _BEGIN */
#define u_width 1

/*
 * Create external references here.  
 *
 */
/* %%%-SFUNWIZ_wrapper_externs_Changes_BEGIN --- EDIT HERE TO _END */
/* extern double func(double a); */
/* %%%-SFUNWIZ_wrapper_externs_Changes_END --- EDIT HERE TO _BEGIN */

/*
 * Start function
 *
 */
void PulsePalOut_Start_wrapper(const real_T *port, const int_T p_width0,
			const real_T *channel, const int_T p_width1,
			SimStruct *S)
{
/* %%%-SFUNWIZ_wrapper_Start_Changes_BEGIN --- EDIT HERE TO _END */
PulsePal* PulsePalObject;
PulsePalObject = new PulsePal;
switch ((int)port[0]){
    case 1:PulsePalObject->initialize("COM1");break;
    case 2:PulsePalObject->initialize("COM2");break;
    case 3:PulsePalObject->initialize("COM3");break;
    case 4:PulsePalObject->initialize("COM4");break;
    case 5:PulsePalObject->initialize("COM5");break;
    case 6:PulsePalObject->initialize("COM6");break;
    case 7:PulsePalObject->initialize("COM7");break;
    case 8:PulsePalObject->initialize("COM8");break;
    case 9:PulsePalObject->initialize("COM9");break;
    case 10:PulsePalObject->initialize("COM10");break;
    case 11:PulsePalObject->initialize("COM11");break;
}

// Get handshake byte and return firmware version
uint32_t FV = PulsePalObject->getFirmwareVersion(); // Exchanges a "handshake byte" and then retrieves the current firmware version from PulsePal
mexPrintf("Current firmware version %i", FV);

// Set some parameters for output channels 1 and 3
PulsePalObject->setPhase1Voltage((int)channel[0], 5);          // Set voltage to 5V on output channel 1
PulsePalObject->setPhase1Duration((int)channel[0], .001);      // Set duration to 1ms
PulsePalObject->setInterPulseInterval((int)channel[0], .001);  // Set interval to 1ms
PulsePalObject->setPulseTrainDuration((int)channel[0], 0);     // Set train duration to 0s
ssSetUserData(S,PulsePalObject);
/* %%%-SFUNWIZ_wrapper_Start_Changes_END --- EDIT HERE TO _BEGIN */
}
/*
 * Output function
 *
 */
void PulsePalOut_Outputs_wrapper(const real_T *u,
			const real_T *port, const int_T p_width0,
			const real_T *channel, const int_T p_width1,
			SimStruct *S)
{
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_BEGIN --- EDIT HERE TO _END */
if (u){
    PulsePal* PulsePalObject = (PulsePal*)ssGetUserData(S);
    PulsePalObject->triggerChannel((int)channel[0]);
}
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_END --- EDIT HERE TO _BEGIN */
}

/*
 * Terminate function
 *
 */
void PulsePalOut_Terminate_wrapper(const real_T *port, const int_T p_width0,
			const real_T *channel, const int_T p_width1,
			SimStruct *S)
{
/* %%%-SFUNWIZ_wrapper_Terminate_Changes_BEGIN --- EDIT HERE TO _END */
/*
 * Custom Terminate code goes here.
 */
PulsePal* PulsePalObject = (PulsePal*)ssGetUserData(S);
PulsePalObject->end();
delete PulsePalObject;
/* %%%-SFUNWIZ_wrapper_Terminate_Changes_END --- EDIT HERE TO _BEGIN */
}

