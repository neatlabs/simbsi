/*
 * 
 * Abstract:
 *       C-file S-function for reading data from the Lab Streaming Layer (LSL). 
 *	 
 * This file is part of the SimBSI project, see more at https://bitbucket.org/neatlabs/simbsi/wiki/Home.
 *
 * This code is provided onder a BSD type license (educational, research and non-profit purposes). See 
 * other terms and conditions in https://bitbucket.org/neatlabs/simbsi/src/master/license.txt.
 *
 * Author: Alejandro Ojeda, 2018, Neural Engineering and Translation Labs, University of California San Diego
 */


#define S_FUNCTION_NAME  LSLInlet
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <stdio.h>
#include <stdlib.h>
#include "lsl_c.h"
#include "simbsi.h"


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *   Setup sizes of the various vectors.
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 1);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }
    //ssSetSFcnParamTunable(S,0,false);
    
    int_T buflen = mxGetN(ssGetSFcnParam(S, 0))*sizeof(mxChar)+1;
    char *streamName = calloc(mxGetN(ssGetSFcnParam(S, 0)), sizeof(mxChar));
    if (streamName == NULL){
        ssSetErrorStatus(S,"ERROR: Out of memmory");
        return;
    }
    int_T status = mxGetString(ssGetSFcnParam(S, 0), streamName,buflen);

    mexPrintf("Now waiting for stream %s... ", streamName);
    lsl_streaminfo info;
	lsl_resolve_byprop(&info,1, "name",streamName, 1, LSL_FOREVER);
    mexPrintf("stream found.\n");
    char *streamType = lsl_get_type(info);
    mexPrintf("Type %s\n", streamType);

    int numberOfChannels = lsl_get_channel_count(info);
    mexPrintf("Number of channels %d\n", numberOfChannels);

    // Set ports
    if (!ssSetNumInputPorts(S, 0)) return;
    if (!ssSetNumOutputPorts(S,2)) return;

    ssSetOutputPortWidth(S, 0, numberOfChannels);
    ssSetOutputPortDataType(S, 0, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 0, COMPLEX_NO);
    ssSetOutputPortOptimOpts(S, 0, SS_NOT_REUSABLE_AND_LOCAL);
    
    ssSetOutputPortWidth(S, 1, 1);
    ssSetOutputPortDataType(S, 1, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 1, COMPLEX_NO);
    ssSetOutputPortOptimOpts(S, 1, SS_NOT_REUSABLE_AND_LOCAL);

    // Set other Simulink parameters
    ssSetNumSampleTimes(S, -1);

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_WORKS_WITH_CODE_REUSE));
    
    // Define run-time user data
    InletUserData* userData = calloc(sizeof(InletUserData),1);
    if (userData == NULL){
        ssSetErrorStatus(S,"ERROR: Out of memmory");
        return;
    }
    userData->name = streamName;   
    userData->samplingRate = lsl_get_nominal_srate(info);
    userData->chunkSize = 1;
    userData->numberOfChannels = numberOfChannels;
    if (strcmp(streamType,"markers")==0 | strcmp(streamType,"Markers")==0){
        userData->isMarkerType = true;
        userData->timeout = 0;
    }
    else{
        userData->isMarkerType = false;
        userData->timeout = LSL_FOREVER;
    }
    
    userData->inlet = lsl_create_inlet(info, 300, LSL_NO_PREFERENCE, 0);
    mexPrintf("Inlet %s created\n", streamName);
    ssSetUserData(S,userData);

    lsl_destroy_streaminfo(info);
    
    // Open inlet
    int errcode;
	  lsl_open_stream(userData->inlet, LSL_FOREVER, &errcode);
    if (errcode){
        static char msg[255];
        sprintf(msg,"Error calling function lsl_open_stream. Error code %d.",errcode);
        ssSetErrorStatus(S,msg);
    }
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    InletUserData* userData = (InletUserData*)ssGetUserData(S);
    ssSetSampleTime(S, 0, 1.0/userData->samplingRate);
    ssSetSampleTime(S, 1, 1.0/userData->samplingRate);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetOffsetTime(S, 1, 0.0);
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *    Read data sample from LSL.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    int errcode;
    InletUserData* userData = (InletUserData*)ssGetUserData(S);
    userData->DATA = ssGetOutputPortRealSignal(S,0);
    if (userData->isMarkerType)
        userData->DATA[0] = 0;
    userData->timeStamps = (real_T *)ssGetOutputPortRealSignal(S,1);
    *userData->timeStamps = lsl_pull_sample_d(userData->inlet, userData->DATA, userData->numberOfChannels, userData->timeout, &errcode);
    if (errcode){
        static char msg[255];
        sprintf(msg,"Error calling function lsl_pull_sample_f. Error code %d.",errcode);
        ssSetErrorStatus(S,msg);
    }
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    Release resources.
 */
static void mdlTerminate(SimStruct *S)
{
    InletUserData* userData = (InletUserData*)ssGetUserData(S);
    lsl_destroy_inlet(userData->inlet);
    free(userData->name);
    free(userData);
    mexPrintf("Inlet %s destroyed\n", userData->name);
}

#ifdef MATLAB_MEX_FILE
#include "simulink.c"
#else
#include "cg_sfun.h"
#endif
