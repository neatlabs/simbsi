#include "mex.h"
#include "matrix.h"
#include "lsl_c.h"
 

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    lsl_inlet inlet; 
    lsl_streaminfo info[100];
    lsl_streaminfo full_info;
    int n = lsl_resolve_all(info, 100, 0.5);   
    mxArray* streamName = mxCreateCellMatrix(n, 1);
    mxArray* streamType = mxCreateCellMatrix(n, 1);
    mxArray* channelLabels = mxCreateCellMatrix(n, 1);
    mxArray* srate = mxCreateDoubleMatrix( n, 1, mxREAL);
    mxArray* channelCount = mxCreateDoubleMatrix( n, 1, mxREAL);
    double* sr = mxGetPr(srate);
    double* nbchan = mxGetPr(channelCount);
    
    int errcode;
    lsl_xml_ptr chn;
    mxArray* theseChannelLabels;
    
    for (mwIndex i=0; i<n; i++){
        mxSetCell(streamName, i, mxCreateString(lsl_get_name(info[i])));
        mxSetCell(streamType, i, mxCreateString(lsl_get_type(info[i])));
        sr[i] = lsl_get_nominal_srate(info[i]);
        nbchan[i] = lsl_get_channel_count(info[i]);
        
        inlet = lsl_create_inlet(info[i], 360, LSL_NO_PREFERENCE, 1);
        full_info = lsl_get_fullinfo(inlet,LSL_FOREVER,&errcode);
        theseChannelLabels = mxCreateCellMatrix(nbchan[i],1);
        chn = lsl_child(lsl_child(lsl_get_desc(full_info),"channels"),"channel");
        for(mwIndex c=0;c<nbchan[i];c++){
            mxSetCell(theseChannelLabels, c, mxCreateString(lsl_child_value_n(chn,"label")));
            chn = lsl_next_sibling(chn);
        }
        mxSetCell(channelLabels, i, theseChannelLabels);
        
        lsl_destroy_streaminfo(full_info);
        lsl_destroy_inlet(inlet);
        lsl_destroy_streaminfo(info[i]);
    }
    plhs[0] = streamName;
    plhs[1] = streamType;
    plhs[2] = srate;
    plhs[3] = channelCount;
    plhs[4] = channelLabels;
}
