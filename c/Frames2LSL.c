/*
 * 
 * Abstract:
 *       C-file S-function for sending frame count of acquired video to the Lab Streaming Layer (LSL). 
 *	 
 * This file is part of the SimBSI project, see more at https://bitbucket.org/neatlabs/simbsi/wiki/Home.
 *
 * This code is provided onder a BSD type license (educational, research and non-profit purposes). See 
 * other terms and conditions in https://bitbucket.org/neatlabs/simbsi/src/master/license.txt.
 *
 * Author: Alejandro Ojeda, 2018, Neural and Translation Labs, University of California San Diego
 */


#define S_FUNCTION_NAME  Frames2LSL
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lsl_c.h"
#include "simbsi.h"

/*================*
 * Build checking *
 *================*/
#if !defined(MATLAB_MEX_FILE)
/*
 * This file cannot be used directly with the Real-Time Workshop. However,
 * this S-function does work with the Real-Time Workshop via
 * the Target Language Compiler technology. See 
 * matlabroot/toolbox/simulink/blocks/tlc_c/timestwo.tlc   for the C version
 * matlabroot/toolbox/simulink/blocks/tlc_ada/timestwo.tlc for the Ada version
 */
# error This_file_can_be_used_only_during_simulation_inside_Simulink
#endif


/* Function: getID =============================================== 
 * Abstract: 
 *   Generates random source id.
 */
void getID(const int maxLength, char *id)
{
    static const char alphanum[] = "abcdefghijklmnopqrstuvwxyz0123456789";
    //srand(time(NULL));
    for(int i=0; i<maxLength; i++)
        id[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    id[maxLength-1] = 0;
}


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *   Setup sizes of the various vectors.
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 2);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }
    ssSetSFcnParamTunable(S,0,false);
    ssSetSFcnParamTunable(S,1,false);
        
    int *index = (int *)calloc( 1, sizeof(int));
    char *streamName;
    float fps;
    char streamType[] = "videostream";
    char channelLabels[] = "frame";
    getID(LSLSourceIDLength, LSLSourceID[CurrentNumberOfSimLSLOutlets]);
    int numberOfChannels = 1;
    int_T buflen;
    int_T status;
    lsl_xml_ptr desc, chn, chns;/* some xml element pointers */ 
    
    // Parse input parameters 
    buflen = mxGetN(ssGetSFcnParam(S, 0))*sizeof(mxChar)+1;
    streamName = calloc(mxGetN(ssGetSFcnParam(S, 0)), sizeof(mxChar));
    status = mxGetString((ssGetSFcnParam(S, 0)), streamName,buflen);
    mexPrintf("Stream name: %s\n", streamName);
      
    fps = *mxGetPr(ssGetSFcnParam(S, 1));
    mexPrintf("FPS: %f\n", fps);
    mexPrintf("Source ID: %s\n", LSLSourceID[CurrentNumberOfSimLSLOutlets]);
    
    // Configure ports
    if (!ssSetNumInputPorts(S, 2)) return;
    ssSetInputPortWidth(S, 0, 1);
    ssSetInputPortWidth(S, 1, 1);
    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortDirectFeedThrough(S, 1, 1);
    if (!ssSetNumOutputPorts(S,0)) return;

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, SS_OPTION_CALL_TERMINATE_ON_EXIT | 
                    SS_OPTION_EXCEPTION_FREE_CODE |
                    SS_OPTION_ALLOW_PORT_SAMPLE_TIME_IN_TRIGSS);
    
    /* declare a streaminfo */
    lsl_streaminfo info = lsl_create_streaminfo(
            streamName, 
            streamType,
            numberOfChannels,
            fps,
            cft_float32,
            LSLSourceID[CurrentNumberOfSimLSLOutlets]);
    
    // Add stream's metadata
    desc = lsl_get_desc(info);
	chns = lsl_append_child(desc,"channels");
    chn = lsl_append_child(chns,"channel");
    lsl_append_child_value(chn,"label",channelLabels);
    lsl_append_child_value(chn,"type",streamType);
    
    *index = CurrentNumberOfSimLSLOutlets;
    int ind = *index;
    SimLSLFrameCounter[ind] = 0;
    SimLSLPushBuffer[ind] = (float *)calloc( numberOfChannels, sizeof(float));
    for(int i=0;i<numberOfChannels;i++)
        SimLSLPushBuffer[ind][i] = 0;
    SimLSLOutlet[ind] = lsl_create_outlet(info,0,360);
    CurrentNumberOfSimLSLOutlets += 1;
    mexPrintf("Number of outlets: %d\n", CurrentNumberOfSimLSLOutlets);
    lsl_push_sample_f(SimLSLOutlet[ind], SimLSLPushBuffer[ind]);
    mexPrintf("Outlet %d created\n\n",ind);
    ssSetUserData(S,index);
    free(streamName);
    lsl_destroy_streaminfo(info);
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME); /*INHERITED_SAMPLE_TIME*/
    ssSetSampleTime(S, 1, INHERITED_SAMPLE_TIME); /*INHERITED_SAMPLE_TIME*/	
    ssSetOffsetTime(S, 0, 0.0);
    ssSetOffsetTime(S, 1, 0.0);
}


/* Function: mdlOutputs =======================================================
 * Abstract:
 *    Output an incremental frame count.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    int numberOfChannels = 1;
    int index = *(int*)ssGetUserData(S);
    SimLSLFrameCounter[index] +=1;
    InputRealPtrsType tPtrs = ssGetInputPortRealSignalPtrs(S,1);
    int errcode = lsl_no_error;
    if (*tPtrs[0]==0){
        errcode = lsl_push_sample_ft(SimLSLOutlet[index], &SimLSLFrameCounter[index], *tPtrs[0]);
    }
    else{
        errcode = lsl_push_sample_f(SimLSLOutlet[index], &SimLSLFrameCounter[index]);
    }
    //lsl_have_consumers(userData->outlet)){
    if (errcode){
        static char msg[255];
        if (*tPtrs[0]==0)
            sprintf(msg,"Error calling function lsl_push_sample_ft. Error code %d.",errcode);
        else
            sprintf(msg,"Error calling function lsl_push_sample_f. Error code %d.",errcode);
        ssSetErrorStatus(S,msg);
    }
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    Release resources.
 */
static void mdlTerminate(SimStruct *S)
{   
    int index = *(int *)ssGetUserData(S);
    lsl_destroy_outlet(SimLSLOutlet[index]);
    mexPrintf("Outlet %d destroyed\n",index);
    free(SimLSLPushBuffer[index]);
    CurrentNumberOfSimLSLOutlets -= 1;
}


//#if defined(MATLAB_MEX_FILE)
//#define MDL_RTW
/* Function: mdlRTW ===========================================================
 * Abstract:
 *	Since we've declared all are parameters as non-tunable, we need
 *	only provide this routine so that they aren't written to the model.rtw
 *	file. The values of the parameters are implicitly encoded in the
 *	sample times.
 */
//static void mdlRTW(SimStruct *S)
//{
//}
//#endif /* MDL_RTW */


#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
