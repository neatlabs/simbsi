#include "lsl_c.h"
#include <time.h>
#include "simstruc.h"


// LSL Inlet
typedef struct InletUserData{
    lsl_inlet inlet;
    char* name;
    double samplingRate;
    int chunkSize;
    int numberOfChannels;
    bool isMarkerType;
    double* DATA;
    double** data;
    double* timeStamps;
    double timeout;
}InletUserData;

// LSL Outlet
typedef struct OutletUserData{
    lsl_outlet outlet;
    char* name;
    double samplingRate;
    int chunkSize;
    int numberOfChannels;
    bool isMarkerType;
    double* DATA;
    double** data;
    double* sample;
    double* timeStamps;
}OutletUserData;

// Frames2LSL
// float SimLSLFrameCounter[100];
