# SimBSI #

Welcome to SimBSI! SimBSI is an open-source graphical environment for the rapid prototyping of animal and human brain-computer interfaces (BCIs). SimBSI is designed as a library on top of the graphical programming environment of [Simulink](https://www.mathworks.com/products/simulink.html) with three goals in mind:

1. To provide a flexible cognitive platform for developing human and animal experiments by using Simulink's [Stateflow](https://www.mathworks.com/products/stateflow.html) programming. 

2. To allow for flexible data acquisition by including multiplatform drivers for standard instrument communication protocols (including the [Lab Streaming Layer](https://github.com/sccn/labstreaminglayer)).

3. To allow for real-time analysis and control of neural circuit dynamics by leveraging Simulink's [DSP](https://www.mathworks.com/products/dsp-system.html) and [Control](https://www.mathworks.com/products/simcontrol.html) toolboxes in addition to a customized neuroimaging module. 

See more details on the [Wiki](https://bitbucket.org/neatlabs/simbsi/wiki/Home).