classdef RandomSelector < matlab.System &...
        matlab.system.mixin.Propagates &...
        matlab.system.mixin.CustomIcon
    % untitled2 Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties(Nontunable)
        fixedLocation = 0; % Location on the screen that won't be shuffled
    end

    properties(DiscreteState)

    end

    % Pre-computed constants
    properties(Access = private)

    end

    methods(Access = protected)
   
        function icon = getIconImpl(~)
            icon = '';
        end
       
        function setupImpl(~)
            rng('default');
        end
        
        function [sz1, sz2] = getOutputSizeImpl(obj)
            sz1 = propagatedInputSize(obj, 1);
            if isempty(sz1)
                sz2 = [];
            else
                sz1 = [sz1(1) sz1(2)*sz1(4) sz1(3)];
                sz2 = 1;
            end
        end
        function [fz1, fz2] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
        end
        function [dt1, dt2] = getOutputDataTypeImpl(obj)
            dt1 = propagatedInputDataType(obj, 1);
            dt2 = 'logical';
        end
        function [cp1, cp2] = isOutputComplexImpl(obj)
            cp1 = propagatedInputComplexity(obj, 1);
            cp2 = propagatedInputComplexity(obj, 1);
        end
        
        function [y, isGo] = stepImpl(obj,u, goImg, nogoImg, goProb)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            numberOfInputs = size(u,4);
            dim = size(u);
            y = u;
            if obj.fixedLocation>0
                numberOfInputs = numberOfInputs-1;
                ind = setdiff(1:size(u,4),obj.fixedLocation);
            else
                ind = 1:numberOfInputs;
            end
            position = randperm(numberOfInputs, 1);
            if rand < goProb
                y(:,:,:,ind(position)) = goImg;
                isGo = true;
            else
                y(:,:,:,ind(position)) = nogoImg;
                isGo = false;
            end
            y = permute(y, [1 2 4 3]);
            y = reshape(y, [dim(1) dim(2)*dim(4) dim(3)]);
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
    end
end
