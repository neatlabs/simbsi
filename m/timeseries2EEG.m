function EEG = timeseries2EEG(tsObj, setname, channelLabels, channelPositions)
if nargin < 2
    setname = 'SimBCI';
end
EEG = eeg_emptyset;
EEG.setname = setname;
EEG.times = tsObj.Time*1000;
dim = size(tsObj.Data);
if dim(1) == 1
    data = squeeze(tsObj.Data);
else
    data = tsObj.Values.Data;
end
[EEG.nbchan, EEG.pnts] = size(data);
EEG.data = data;
EEG.srate = round(median(1./diff(tsObj.Time)));
EEG.xmin  = EEG.times(1);
EEG.xmax  = EEG.times(end);
EEG.trials = 1;


% Add channel information (if any)
if nargin < 3, return;end
if nargin < 4
    if EEG.nbchan ~= length(channelLabels)
        disp('EEG.nbchan is diffrent than length(channelLabels). You got to fix this to be able to add channel locations.')
        return;
    end
    hmfile = which('head_modelColin27_5003_Standard-10-5-Cap339.mat');
    if ~exist(hmfile,'file')
        disp('Cannot look up the channel positions. Try adding the headModel (https://github.com/aojeda/headModel) toolbox to the path.');
        return;
    end
    hm = headModel.loadFromFile(hmfile);
    [~,loc1,loc2] = intersect(hm.labels, channelLabels, 'stable');
    if isempty(loc1)
        disp('The labels you entered are not in the head model template. Please make sure to label your channels following the 10-20 system.');
        return;
    end
    channelPositions = nan(EEG.nbchan,3);
    channelPositions(loc2,:) = hm.channelSpace(loc1,:);
end

if EEG.nbchan ~= length(channelPositions)
    disp('EEG.nbchan is diffrent than length(channelPositions). You got to fix this to be able to add channel locations.')
    return;
end
EEG.chanlocs = repmat(struct('labels',[],'type',[],'X',[],'Y',[],'Z',[],'radius',[],'theta',[]),EEG.nbchan,1);
for k=1:EEG.nbchan
    EEG.chanlocs(k).X = channelPositions(k,1);
    EEG.chanlocs(k).Y = channelPositions(k,2);
    EEG.chanlocs(k).Z = channelPositions(k,3);
    if ~isnan(channelPositions(k,1))
        [EEG.chanlocs(k).theta, EEG.chanlocs(k).radius] = cart2pol(channelPositions(k,1), channelPositions(k,2), channelPositions(k,3));
        EEG.chanlocs(k).theta = -EEG.chanlocs(k).theta*180/pi;
    end
    EEG.chanlocs(k).type = 'EEG';
    EEG.chanlocs(k).labels = channelLabels{k};
end
EEG = eeg_checkset(EEG);