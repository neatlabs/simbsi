classdef MVAR < matlab.System & matlab.system.mixin.Propagates
    % MVAR Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    properties
        
    end
    
    properties(Nontunable)
        order = 1;          % Model order
        maxIterations = 50; % Maximum number of iterations
        tolerance = 1e-1;   % Convergence criteria
    end
    
    properties(DiscreteState)
    end

    % Pre-computed constants
    properties(Access = private)
        Ut;
        s
        s2;
        V;
        Ny=0;
        loss;
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            obj.loss = zeros(obj.maxIterations,1);
        end
        
        function computeSVD(obj, H)
            [U,S,obj.V] = svd(H,'econ');
            obj.Ut = U';
            obj.s = diag(S);
            obj.s2 = obj.s.^2;
            obj.Ny = min(size(H));
        end
        
        function [A, FConn, L] = stepImpl(obj,X)
            %X(isnan(X(:))) = 0;
            [Y, H] = obj.formEmbedding(X');
            obj.computeSVD(H);
            UtY = obj.Ut*Y;
            y2 = UtY.^2;
            S = [obj.s2 ones(obj.Ny,1)];
            phi = abs(mean((S'*S)\S'*y2,2));
            lambda = phi(2);
            gamma_F = phi(1);
            gamma_F(gamma_F < 1e-3) = 1e-3;
            obj.loss = obj.loss*0;
            k=1;
            for k=1:obj.maxIterations
                psi = gamma_F*obj.s2 + lambda;
                psi2 = psi.^2;
                lambda   = lambda*sum(mean(bsxfun(@times,y2, 1./psi2),2))/(eps+sum(1./psi));
                gamma_F  = gamma_F*sum(mean(bsxfun(@times,y2,obj.s2./psi2),2))/(eps+sum(obj.s2./psi));
                obj.loss(k) = sum(log(psi))+sum(mean(bsxfun(@rdivide,y2,psi),2));
                if k> 1 && abs(diff(fliplr(obj.loss(k-1:k)))) < obj.tolerance
                    break;
                end
            end
            A = (gamma_F*bsxfun(@times,obj.V,(obj.s./psi)')*UtY)';
            L = obj.loss(k);
            E = Y' - A*H';
            FConn = corr(E');
        end

        function resetImpl(~)
            % Initialize / reset discrete-state properties
        end
        
        function [sz1, sz2, sz3] = getOutputSizeImpl(obj)
            sz1 = propagatedInputSize(obj, 1);
            if ~isempty(sz1)
                sz1 = [sz1(2) sz1(2)*obj.order];
                sz2 = [sz1(1) sz1(1)];
                sz3 = 1;
            else
                sz2 = [];
                sz3 = [];
            end
        end
        function [fz1,fz2,fz3] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
        end
        function [dt1,dt2,dt3] = getOutputDataTypeImpl(obj)
            dt1 = propagatedInputDataType(obj, 1);
            dt2 = propagatedInputDataType(obj, 1);
            dt3 = propagatedInputDataType(obj, 1);
        end
        function [cp1,cp2,cp3] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
        end
    
        function [Y,H] = formEmbedding(obj,X)
            [Nx,Nt] = size(X);
            loc = rot90(bsxfun(@plus,(1:obj.order+1)',0:Nt-obj.order-1),2);
            Y = X(:,loc(1,:))';
            H = reshape(permute(reshape(X(:,loc(2:end,:)'),[Nx,(Nt-obj.order),obj.order]),[2 1 3]),(Nt-obj.order),[]);
        end
    end
end
