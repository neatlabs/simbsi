function clear_roi_selection(src,~)
hBlock = src.UserData{1};
hViewer = src.UserData{2};
hViewer.roiLabels = {};
set_param(hBlock,'roi','{}');
end