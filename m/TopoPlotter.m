classdef TopoPlotter < matlab.System % & simevents.SimulationObserver
    % Interpolate EEG sensor data and render them on a 3D model of the head.
    %
    
    % Public, tunable properties
    properties
        colorMap = 'human';     % Colormap
        clim = [-1 1];          % Voltage limits
        autoscale = false;      % Autoscale
    end
    
    properties(Nontunable)
        figTitle = 'Scalp EEG topography';  % Figure title
        labels;
    end
    
    properties(Hidden)
        colorMapSet = matlab.system.StringSet({'human','bipolar','jet','hsv','parula'});
    end
    
    % Pre-computed constants
    properties(Access = private)
        fig;
        ax;
        hScalp;
        hLabels;
        hSensors;
        indz;
        showSensors = true; % Show sensors
        showLabels  = false; % Show labels
        W;
    end
    methods
        function icon = getIconImpl(~)
            rootDir = fileparts(which('SimBSI'));
            icon = matlab.system.display.Icon(fullfile(rootDir,'resources','icons','scalp_viewer.png'));
        end
        function showLabelsBtn(obj, ~)
            obj.showLabels = ~obj.showLabels;
            if obj.showLabels
                set(obj.hLabels,'Visible','on');
            else
                set(obj.hLabels,'Visible','off');
            end
        end
        function showSensorsBtn(obj, ~)
            obj.showSensors = ~obj.showSensors;
            if obj.showSensors
                set(obj.hSensors,'Visible','on');
            else
                set(obj.hSensors,'Visible','off');
            end
        end
    end
    methods(Access=protected)
        function processTunedPropertiesImpl(obj)
            obj.ax.CLim = obj.clim;
            cmap = obj.getCMap();
            colormap(obj.ax, cmap);
            obj.fig.Name = obj.figTitle;
        end
    end
    methods(Access = protected)
        function setupImpl(obj)
            figHandlers = findobj('Tag','TopoPlotter');
            if isempty(figHandlers)
                obj.createFig();
            else
                for k=1:length(figHandlers)
                    if strcmp(figHandlers(k).UserData.figTitle,obj.figTitle)
                        obj.fig = figHandlers(k);
                        obj.ax = findobj(obj.fig,'Type','axes');
                        obj.hLabels = figHandlers(k).UserData.hLabels;
                        obj.hSensors = figHandlers(k).UserData.hSensors;
                        obj.hScalp = figHandlers(k).UserData.hScalp;
                        obj.W = obj.fig.UserData.W;
                        obj.indz = obj.fig.UserData.indz;
                        break;
                    end
                end
                if isempty(obj.fig)
                    obj.createFig();
                end
            end
            obj.fig.UserData.figTitle = obj.figTitle;
        end
        
        function stepImpl(obj,y)
            %F = scatteredInterpolant(obj.channelSpace,y','natural','linear');
            %y = F(obj.scalp.vertices);
            y = obj.W*y';
            y(obj.indz) = 0;
            if obj.autoscale
                y = y/max(abs(y(:)));
            end
            set(obj.hScalp,'FaceVertexCData',y);
        end
        
        function createFig(obj)
            hm = evalin('base','hm');
            obj.indz = hm.scalp.vertices(:,3) < min(hm.channelSpace(:,3)) - 0.1*abs(min(hm.channelSpace(:,3)));
            obj.W = geometricTools.localGaussianInterpolator(hm.channelSpace,hm.scalp.vertices,0.03,true);
            
            obj.fig = figure('Name',obj.figTitle, 'MenuBar','none','ToolBar','figure','Visible','off','Tag','TopoPlotter');
            obj.fig.Position(3:4) = [500 350];
            obj.fig.Visible = 'on';
            obj.ax = axes(obj.fig,'CameraViewAngle',7);
            
            path = fileparts(which('headModel.m'));
            path = fullfile(path,'+vis','icons');
            labelsOn  = imread([path filesep 'labelsOn.png']);
            sensorsOn = imread([path filesep 'sensorsOn.png']);
            
            toolbarHandle = findall(obj.fig,'Type','uitoolbar');
            hcb1 = uitoggletool(toolbarHandle,'CData',labelsOn,'Separator','off','HandleVisibility','off','TooltipString','Labels On/Off','userData',[],'State','off');
            set(hcb1,'OnCallback',@(src,event)showLabelsBtn(obj,hcb1),'OffCallback',@(src, event)showLabelsBtn(obj,hcb1));
            
            hcb2 = uitoggletool(toolbarHandle,'CData',sensorsOn,'Separator','off','HandleVisibility','off','TooltipString','Sensors On/Off','userData',[],'State','off');
            set(hcb2,'OnCallback',@(src,event)showSensorsBtn(obj,hcb2),'OffCallback',@(src, event)showSensorsBtn(obj,hcb2));
            
            
            N = length(hm.labels);
            k = 1.1;
            obj.hLabels = zeros(N,1);
            for it=1:N
                obj.hLabels(it) = text(...
                    'Position',k*hm.channelSpace(it,:),...
                    'String',hm.labels{it},...
                    'Parent',obj.ax);
            end
            set(obj.hLabels,'Visible','off');
            hold(obj.ax,'on');
            obj.hSensors = scatter3(obj.ax,hm.channelSpace(:,1),hm.channelSpace(:,2),...
                hm.channelSpace(:,3),'filled','MarkerEdgeColor','k','MarkerFaceColor','y');
            if ~obj.showSensors
                set(obj.hSensors,'Visible','off');
            end
            obj.hScalp = patch('vertices',hm.scalp.vertices,'faces',hm.scalp.faces,'FaceVertexCData',zeros(size(hm.scalp.vertices,1),1),...
                'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',1,'SpecularColorReflectance',0,...
                'SpecularExponent',25,'SpecularStrength',0.1,'parent',obj.ax);
            axis(obj.ax,'off')
            axis(obj.ax,'equal');
            axis(obj.ax,'vis3d');
            camlight('headlight');
            camlight(obj.ax,90,0);
            camlight(obj.ax,-90,0);
            view(obj.ax,[127 36]);
            colormap(obj.ax, obj.getCMap());
            obj.ax.CLim = obj.clim;
            hold(obj.ax,'off');
            cb = colorbar(obj.ax);
            %cb.Label.String = 'EEG';
            obj.fig.UserData.hLabels = obj.hLabels;
            obj.fig.UserData.hSensors = obj.hSensors;
            obj.fig.UserData.hScalp = obj.hScalp;
            obj.fig.UserData.W = obj.W;
            obj.fig.UserData.indz = obj.indz;
            rotate3d(obj.ax);
            drawnow
        end
        
        function cmap = getCMap(obj)
            if strcmp(obj.colorMap,'bipolar')
                cmap = bipolar(256,0.7);
            elseif strcmp(obj.colorMap,'human')
                cmap = vis.human(256);
            elseif strcmp(obj.colorMap,'jet')
                cmap = jet(256);
            elseif strcmp(obj.colorMap,'hsv')
                cmap = hsv(256);
            elseif strcmp(obj.colorMap,'parula')
                cmap = parula(256);
            else
                cmap = bipolar(256,0.7);
            end
        end
    end
    
end
