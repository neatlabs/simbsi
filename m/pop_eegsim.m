function [data, event] = pop_eegsim(EEG, model)
if nargin < 2, model = 'eegplayer';end
data = double(reshape(EEG.data,EEG.nbchan,[]));
dim = size(data);
if ~isempty(EEG.event)
    uniqueEvents = unique({EEG.event.type},'stable');
    Ne = length(uniqueEvents);
    event = zeros(Ne,dim(2));
    latency = round([EEG.event.latency]);
    for k=1:Ne
        ind = ismember({EEG.event.type}, uniqueEvents{k});
        event(k,latency(ind)) = k;
    end
else
    event = zeros(1,dim(2));
end

load_system(['examples/' model]);
cs = getActiveConfigSet(model);
cs.set_param('StartTime','0');
cs.set_param('StopTime','inf');
cs.set_param('FixedStep',num2str(1/EEG.srate));
cs.set_param('SaveState','off');
cs.set_param('SaveTime','off');

if ~isempty(EEG.chanlocs)
        val = '';
    for k=1:EEG.nbchan
        val = [val ''',''' EEG.chanlocs(k).labels];
    end
    val(end+1) = '''';
    val([1 2]) = [];
    val = ['{' val '}'];
    set_param([model '/Load Head Model 10//20'],'channelLabels',val);
    mx = max(abs(prctile(EEG.data(:),[5 95])));
    set_param([model '/Scalp Viewer'],'clim',['[-' num2str(mx) ',' num2str(mx)  ']']);
else
    warning('Cannot render EEG topography because channel labels are missing\n. Consider commenting out the TopoPlotter block.');
end
open_system(model);
end