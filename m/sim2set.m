function setfile = sim2set(rawFile, channelLabels, srate, nbchan)
[filePath,setname,~] = fileparts(rawFile);
setfile = fullfile(filePath,[setname,'.set']);
load(rawFile,'RawData');
EEG = eeg_emptyset;
EEG.setname = setname;
EEG.srate = srate;
EEG.nbchan = nbchan;
EEG.data = RawData(2:nbchan+1,:); %#ok
EEG.times = RawData(nbchan+2,:)*1000;
EEG.times = EEG.times-EEG.times(1);
EEG.xmin  = EEG.times(1);
EEG.xmax  = EEG.times(end);
EEG.trials = 1;

teeg = RawData(nbchan+2,:);
tmrk = RawData(end,:);

n = length(teeg);
xeeg = 1:n;
mrk = RawData(end-1,:);
indnz = find(mrk);
latency = interp1(teeg,xeeg,tmrk(indnz),'nearest');

rmInd = isnan(latency);
latency(rmInd) = [];
indnz(rmInd) = [];
mrk = mrk(indnz);
Nev = length(mrk);
EEG.event = repmat(struct('latency',0,'type',''),Nev,1);
for i=1:Nev
    EEG.event(i).latency = latency(i);
    EEG.event(i).type = num2str(mrk(i));
    EEG.event(i).urevent = i;
end

if ~isempty(channelLabels)
    try
        if EEG.nbchan ~= length(channelLabels)
            error('EEG.nbchan is diffrent than length(channelLabels). You got to fix this to be able to add channel locations.');
        end
        if ~exist(which('headModel'),'file')
            error('Cannot look up the channel positions. Try adding the headModel (https://github.com/aojeda/headModel) toolbox to the path.');
        end
        hm = headModel.loadDefault;
        [~,loc1,loc2] = intersect(hm.labels, channelLabels, 'stable');
        if isempty(loc2)
            error('The labels you entered are not in the head model template. Please make sure to label your channels following the 10-20 system.');
        end
        channelPositions = nan(EEG.nbchan,3);
        channelPositions(loc2,:) = hm.channelSpace(loc1,:);
        if EEG.nbchan ~= length(channelPositions)
            error('EEG.nbchan is diffrent than length(channelPositions). You got to fix this to be able to add channel locations.');
        end
        
        EEG.chanlocs = repmat(struct('labels',[],'type',[],'X',[],'Y',[],'Z',[],'radius',[],'theta',[]),EEG.nbchan,1);
        for k=1:EEG.nbchan
            EEG.chanlocs(k).X = channelPositions(k,1);
            EEG.chanlocs(k).Y = channelPositions(k,2);
            EEG.chanlocs(k).Z = channelPositions(k,3);
            if ~isnan(channelPositions(k,1))
                [EEG.chanlocs(k).theta, EEG.chanlocs(k).radius] = cart2pol(channelPositions(k,1), channelPositions(k,2), channelPositions(k,3));
                EEG.chanlocs(k).theta = -EEG.chanlocs(k).theta*180/pi;
            end
            EEG.chanlocs(k).type = 'EEG';
            EEG.chanlocs(k).labels = channelLabels{k};
        end
    catch ME
        disp(ME)
    end    
end
EEG = eeg_checkset(EEG);
pop_saveset(EEG,'filepath',filePath,'filename',setname);