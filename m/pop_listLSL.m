function streamList = pop_listLSL(select)
if nargin < 1
    select = false;
end
streamList = {};
[streamName, streamType, srate, nbchan, channelLabels] = ListLSLStreams;
n = length(streamName);
labels = cell(n,1);
for i=1:n
    labels{i} = '';
    m = length(channelLabels{i});
    for j=1:m
        labels{i} = [labels{i} channelLabels{i}{j} ''','''];
    end
    labels{i}(end) = [];
    labels{i}(end) = [];
    labels{i} = ['{''' labels{i} '}'];
    channelLabels{i} = channelLabels{i}';
end

d = cell(n, 5);
for i=1:n
    d{i,1} = streamName{i};
    d{i,2} = streamType{i};
    d{i,3} = srate(i);
    d{i,4} = nbchan(i);
    d{i,5} = labels{i};
end
if select
    ColumnWidth = {50,100 100 100 100 255};
    ColumnName = {'Select','Name','Type','Sampling rate','Channel count','Labels'};
    d = cat(2,num2cell(false(n,1)),d);
else
    ColumnWidth = {100 100 100 100 255};
    ColumnName = {'Name','Type','Sampling rate','Channel count','Labels'};
end
    
fig = figure('ToolBar','none','MenuBar','none','NumberTitle','off','Name','LSL Streams','Visible','Off');
fig.Position(3:4) = [692 259];

tab = uitable(fig,'Position',[4 5 554 250],'ColumnEditable',true,'ColumnWidth',ColumnWidth,'units','normalized');
tab.Position = [0.0043 0.0154 0.9957 0.9653];
tab.Data = d;
tab.ColumnName = ColumnName;
fig.Visible = 'on';
if select
    tab.Position = [0.0043 0.1351 0.9957 0.8456];
    uicontrol(fig,'Position',[507 10 60 20],'String','Select','Style', 'pushbutton', 'TooltipString','Select','Callback',@onSelect);
    uicontrol(fig,'Position',[572 10 60 20],'String','Cancel','Style', 'pushbutton', 'TooltipString','Select','Callback',@onCancel);
    try
        while strcmp(fig.Visible,'on')
            pause(0.25);
        end
    catch
        return
    end
    ind = fig.UserData;
    close(fig);
    streamList = d(ind,2:end);
end

end

function onSelect(src,evnt)
tab = findall(src.Parent,'type','uitable');
ind = find(cell2mat(tab.Data(:,1)));
src.Parent.UserData = ind;
src.Parent.Visible = 'off';
end

function onCancel(src,evnt)
src.Parent.Visible = 'off';
end