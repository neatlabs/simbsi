function signal = event2signal(EEG)
dim = size(EEG.data);
if ~isempty(EEG.event)
    uniqueEvents = unique({EEG.event.type},'stable');
    Ne = length(uniqueEvents);
    if numel(dim) < 3
        signal = zeros(Ne,dim(2));
        latency = round([EEG.event.latency]);
        for k=1:Ne
            ind = ismember({EEG.event.type}, uniqueEvents{k});
            etype = str2double(uniqueEvents{1});
            etype(isnan(etype)) = k;
            signal(k,latency(ind)) = etype;
        end
        signal = sum(signal,1)';
    else
        signal = zeros([Ne,dim(2:end)]);
        for t=1:EEG.trials
            ti = cell2mat(EEG.epoch(t).eventlatency);
            [~,lat] = min((ones(numel(ti),1)*EEG.times - (ti'*ones(1,numel(EEG.times)))).^2,[],2);
            for k=1:Ne
                ind = ismember(EEG.epoch(t).eventtype, uniqueEvents{k});
                etype = str2double(uniqueEvents{1});
                etype(isnan(etype)) = k;
                signal(k,lat(ind)) = etype;
            end
        end
        signal = sum(reshape(signal,Ne,[]),1)';
    end
else
    signal = zeros(prod(dim(2:end)),1);
end