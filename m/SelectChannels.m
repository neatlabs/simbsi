classdef SelectChannels < matlab.System &...
        matlab.system.mixin.Propagates &...
        matlab.system.mixin.CustomIcon
    % untitled Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.


    % Pre-computed constants
    properties
        selection = [];
    end

    methods(Access = protected)
        function icon = getIconImpl(~)
            icon = '';
        end
        function Data = stepImpl(obj, Data)
            Data = Data(:,obj.selection);
        end
        
        function sz = getOutputSizeImpl(obj)
            % Maximum length of linear indices and element vector is the
            % number of elements in the input
            sz = propagatedInputSize(obj, 1);
            sz(2) = length(obj.selection);
            
        end
        function fz = isOutputFixedSizeImpl(~)
            %Both outputs are always variable-sized
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(obj)
            cp = propagatedInputComplexity(obj, 1);
        end
    end
end
