classdef ScalpViewerGL < matlab.System &...
        matlab.system.mixin.Propagates
    % ScalpViewerGL Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.
    
    properties(Access = private)
        indz;
        ny;
        W;
    end
    properties(Access = private)
        autoscale = true;
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            hm = evalin('base','hm');
            obj.indz = hm.scalp.vertices(:,3) < min(hm.channelSpace(:,3)) - 0.1*abs(min(hm.channelSpace(:,3)));
            obj.ny = size(hm.scalp.vertices,1);
            obj.W = geometricTools.localGaussianInterpolator(hm.channelSpace,hm.scalp.vertices,0.03,true);
            obj.W = obj.W';
        end
        
        function y = stepImpl(obj, y)
            y = y*obj.W;
            y(:,obj.indz) = 0;
            if obj.autoscale
                y = y/(max(abs(y(:)))+eps);
            end
        end
        
        function sz = getOutputSizeImpl(obj)
            sz = propagatedInputSize(obj, 1);
            if isempty(obj.ny)
                hm = evalin('base','hm');
                obj.ny = size(hm.scalp.vertices,1);
            end
            sz = [sz(1) obj.ny];
        end
        function fz = isOutputFixedSizeImpl(~)
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(obj)
            cp = propagatedInputComplexity(obj, 1);
        end
    end
end
