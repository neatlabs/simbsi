classdef ConnMatrixViewer < matlab.System % & simevents.SimulationObserver
    % Interpolate EEG sensor data and render them on a 3D model of the head.  
    %
    
    % Public, tunable properties
    properties
        roi = {};          % ROI labels   
        clim = [-1 1];     % Clim
        sort = false;      % Sort L/R connections
    end
    
    properties(Nontunable)
        figTitle = '';  % Figure title
    end

    properties(DiscreteState)

    end

    % Pre-computed constants
    properties(Access = private)
        hm;
        fig;
        ax;
        hImg;
        ind;
    end

    methods(Access=protected)
        function processTunedPropertiesImpl(obj)
            obj.ax.CLim = obj.clim;
            title(obj.ax,obj.figTitle);
            n = length(obj.roi);
            obj.ind = 1:n;
            if obj.sort
                obj.ind = reshape(obj.ind,2,[])';
                obj.ind = obj.ind(:);
            end
            obj.ax.YTickLabel = obj.roi(obj.ind);
            obj.ax.XTickLabel = obj.roi(obj.ind);
        end
    end
    methods(Access = protected)
        function setupImpl(obj)
            figHandlers = findobj('Tag','ConnMatrixViewer');
            if ~isempty(figHandlers)
                for k=1:length(figHandlers)
                    if strcmp(figHandlers(k).UserData,obj.figTitle)
                        obj.fig = figHandlers(k);
                        obj.ax = findobj(obj.fig,'Type','axes');
                        break;
                    end
                end
            end
            if isempty(obj.fig)
                obj.fig = figure('Color',0.15*ones(1,3),'UserData',obj,'Tag','ConnMatrixViewer','Visible','off');
                obj.fig.Position(3:4) = [500 350];
                obj.fig.Visible = 'on';
                obj.ax = axes(obj.fig);
            end
            obj.fig.UserData = obj.figTitle;
            dcmHandle = datacursormode(obj.fig);
            dcmHandle.SnapToDataVertex = 'off';
            set(dcmHandle,'UpdateFcn',@(src,event)showConnection(obj,event));
            dcmHandle.Enable = 'off';
        end

        function stepImpl(obj,C)    
            if isempty(obj.hImg)
                n = length(obj.roi);
                obj.ind = 1:n;
                if obj.sort
                    obj.ind = reshape(obj.ind,2,[])';
                    obj.ind = obj.ind(:);
                end
                obj.hImg = imagesc(obj.ax,C(obj.ind,obj.ind));
                if ~isempty(obj.roi)
                    n = length(obj.roi);
                    obj.ax.YTick = 1:n;
                    obj.ax.XTick = 1:n;
                    obj.ax.YTickLabel = obj.roi(obj.ind);
                    obj.ax.XTickLabel = obj.roi(obj.ind);
                    xtickangle(obj.ax,30);
                end
                ylabel(obj.ax,'To');
                xlabel(obj.ax,'From');
                title(obj.ax,obj.figTitle);
                set(obj.ax,'Color','k','XColor','w','YColor','w');
                colormap(obj.ax, bipolar(256,0.7));
                obj.ax.CLim = obj.clim;
                cb = colorbar(obj.ax);
                cb.Label.String = 'Connectivity';
                drawnow
            else
                try %#ok
                    set(obj.hImg,'CData',C);
                end
            end
        end
        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
        function output_txt = showConnection(obj,eventObj)
            pos = get(eventObj,'Position');
            roi_i = obj.roi{obj.ind(pos(2))};
            roi_j = obj.roi{obj.ind(pos(1))};
            output_txt = ['(' roi_i ', ' roi_j ', ' num2str(eventObj.Target.CData(pos(2),pos(1))) ')'];
        end
    end
end
