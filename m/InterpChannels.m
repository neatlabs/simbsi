classdef InterpChannels < matlab.System & matlab.system.mixin.Propagates &...
    matlab.system.mixin.CustomIcon
    
    % InterpChannels Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties(Nontunable)
        channelSpace = [];
        ind = zeros(0,1);
        indi = zeros(0,1);
    end

    properties(DiscreteState)

    end

    % Pre-computed constants
    properties(Access = private)
        prc = 0.05;
        W;
    end

    methods(Access = protected)
        
        function icon = getIconImpl(~)
            icon = '';
        end
        
        function setupImpl(obj)
            n = size(obj.channelSpace,1);
            obj.W = zeros(n);
            for it=1:n
                d = sum(bsxfun(@minus,obj.channelSpace,obj.channelSpace(it,:)).^2,2);
                [~,loc] = sort(d);
                d(loc(2:5));
                obj.W(it,loc(2:5)) = d(loc(2:5))/sum(d(loc(2:5)));
            end
        end

        function y = stepImpl(obj,y)
            y(obj.indi) = y(obj.ind)*obj.W(obj.ind,obj.indi);
        end

        function sz = getOutputSizeImpl(obj)
            sz = propagatedInputSize(obj, 1);
        end
        function fz = isOutputFixedSizeImpl(~)
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(obj)
            cp = propagatedInputComplexity(obj, 1);
        end
    end
end
