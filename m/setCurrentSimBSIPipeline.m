function setCurrentSimBSIPipeline(srate)
try
    cs = getActiveConfigSet(gcs);
    cs.set_param('StartTime','0');
    cs.set_param('StopTime','inf');
    cs.set_param('SolverType','fixed-step');
    if srate>0
        cs.set_param('FixedStep',num2str(1/srate));
    else
        cs.set_param('FixedStep','0.001');
    end
    cs.set_param('DSMLogging','off');
    cs.set_param('SignalLogging','off');
    cs.set_param('SaveOutput','off');
    cs.set_param('SaveState','off');
    cs.set_param('SaveTime','off');
end
end