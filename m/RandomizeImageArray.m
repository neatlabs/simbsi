classdef RandomizeImageArray < matlab.System &...
        matlab.system.mixin.Propagates &...
        matlab.system.mixin.CustomIcon
    % untitled2 Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties(Nontunable)
        ncol = 5; % Number of columns
        nrow = 2; % Number of rows
    end

    properties(DiscreteState)

    end

    % Pre-computed constants
    properties(Access = private)
        position;
        screenSize = [640, 268];
    end

    methods(Access = protected)
   
        function icon = getIconImpl(~)
            icon = '';
        end
       
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
            obj.position = zeros(1,obj.ncol*obj.nrow);
            rng('default');
        end
        
        function [sz1, sz2, sz3, sz4] = getOutputSizeImpl(obj)
            sz1 = propagatedInputSize(obj, 1);
            sz2 = sz1;
            sz3 = sz1;
            sz4 = [];
            if ~isempty(sz1)
                sz1 = [640, 268];
                sz2 = sz1;
                sz3 = sz1;
                sz4 = 1;
            end
        end
        function [fz1, fz2, fz3, fz4] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
        end
        function [dt1, dt2, dt3, dt4] = getOutputDataTypeImpl(obj)
            dt1 = propagatedInputDataType(obj, 1);
            dt2 = dt1;
            dt3 = dt1;
            dt4 = 'logical';
        end
        function [cp1, cp2, cp3, cp4] = isOutputComplexImpl(obj)
            cp1 = propagatedInputComplexity(obj, 1);
            cp2 = propagatedInputComplexity(obj, 1);
            cp3 = propagatedInputComplexity(obj, 1);
            cp4 = propagatedInputComplexity(obj, 1);
        end
        
        function [R, G, B, isCongruent] = stepImpl(obj,imgIn1, imgIn2, show, nItems, congruentProb)
            isCongruent = false;
            imgOut = imgIn1;
            dim = size(imgIn1);
            numberOfInputs = dim(end);
            
            if show==1 % Show Stm 1
                ind = randperm(numberOfInputs, numberOfInputs-nItems);
                imgOut(:,:,:,ind) = 0;
                obj.position(end-nItems+1:end) = setdiff(1:numberOfInputs,sort(ind));
            
            elseif show==2 % Show Stm 2
                imgOut(:) = 0;
                if any(obj.position~=0) 
                    isCongruent = rand < congruentProb;
                    if isCongruent
                        ind = obj.position(end-nItems+1:end);
                        indMask = ind(unidrnd(nItems,1));
                    else
                        ind = setdiff(1:numberOfInputs,obj.position(end-nItems+1:end));
                        indMask = ind(unidrnd(length(ind),1));
                    end
                    imgOut(:,:,:,indMask) = imgIn2;
                end
                obj.position(:) = 0;
            elseif show == 0 % Blank screen
                imgOut(:) = 0;
            end
            tmp1 = reshape(imgOut, [dim(1:3) obj.nrow obj.ncol]);
            tmp2 = permute(tmp1, [1 4 2 5 3]);
            IOut = permute(reshape(tmp2,[dim(1)*obj.nrow, dim(2)*obj.ncol,3]),[2 1 3]);
            % IOut = imresize(tmp3,[640, 245]);

            R = IOut(:,:,1);
            G = IOut(:,:,2);
            B = IOut(:,:,3);
        end
        
        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
    end
end
