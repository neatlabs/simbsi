classdef SpectralViewer < matlab.System
    % SpectralViewer Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        yMin = -10;     % Y-limits (Minimum)
        yMax = 10;      % Y-limits (Maximum)
        freqMin = 0;    % Minimum frequency;
        freqMax = inf;  % Maximum frequency;
    end
    
    properties(Nontunable)
        figTitle = 'Power Spectral Density'; % Title
        figYLabel = ''; % Y-label
    end

    % Pre-computed constants
    properties(Access = private)
        fig = [];    
        ax = [];
        spec = [];
        isAlive = true;
        skip = 1;
        skipCounter = 1;
        freqInd;
        Freq;
    end

    methods(Access = protected)
        function processTunedPropertiesImpl(obj)
            if ~isvalid(obj.fig)
                return;
            end 
           obj.ax.YLim = [obj.yMin obj.yMax]; 
           title(obj.ax,obj.figTitle,'Color','w');
           ylabel(obj.figYLabel);
           obj.freqInd = find(obj.Freq>=obj.freqMin & obj.Freq<=obj.freqMax);
           obj.ax.XLim = obj.Freq(obj.freqInd([1 end]));
        end
        function setupImpl(obj)
            figHandlers = findobj('Tag','SpectralViewer');
            if ~isempty(figHandlers)
                for k=1:length(figHandlers)
                    if strcmp(figHandlers(k).UserData,obj.figTitle)
                        obj.fig = figHandlers(k);
                        obj.ax = findobj(obj.fig,'Type','axes');
                        break;
                    end
                end
            end
            if isempty(obj.fig)
                obj.fig = figure('Color',0.15*ones(1,3),'UserData',obj,'Tag','SpectralViewer');
                obj.ax = axes();
            end
            obj.fig.UserData = obj.figTitle;
        end

        function stepImpl(obj,u, freq)
            if nargin<3, freq = 1:size(u,1);end
            if isempty(obj.spec)
                cla(obj.ax);
                obj.Freq = freq;
                obj.freqInd = find(obj.Freq>=obj.freqMin & obj.Freq<=obj.freqMax);
                obj.spec = plot(obj.ax,freq(obj.freqInd),u(obj.freqInd,:));
                title(obj.ax,obj.figTitle,'Color','w');
                ylabel(obj.ax,obj.figYLabel);
                xlabel(obj.ax,'Frequency (Hz)');
                set(obj.ax,'Color','k','XColor','w','YColor','w');
                obj.ax.XLim = [obj.Freq(obj.freqInd(1)) obj.Freq(obj.freqInd(end))];
                obj.ax.YLim = [obj.yMin obj.yMax];
                grid(obj.ax,'on');
            elseif obj.skipCounter == obj.skip
                for k=1:size(u,2)
                    obj.spec(k).YData = u(:,k);
                    obj.spec(k).XData = freq';
                end
            else
                obj.skipCounter = obj.skipCounter+1;
            end
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
    end
    methods(Access = public)
        function delete(obj)
            obj.isAlive = false;
        end
    end
end
