classdef Flanker < matlab.System & matlab.system.mixin.Propagates
    % Flanker Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        seed = rand();
        imgLeft   = zeros([663 868 3],'uint8');
        imgRight  = zeros([663 868 3],'uint8');
        left = 65361;
        right = 65363;
    end

    % Pre-computed constants
    properties(Access = private)
        state = true;
        direction = 0;%uint16(0);
        stmMatrix = zeros([663 868*3 3],'uint8');
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            rng(obj.seed,'twister');
        end

        function [R,G,B, isCongruent, direction] = stepImpl(obj, congruentProb)
            centerImg = obj.imgLeft;
            if obj.state
                
                % Set arrow direction
                if rand < 0.5
                    obj.direction = 65361;% uint16(65361); % Left
                else
                    obj.direction = 65363;% uint16(65363); % Right
                end
                
                % Set if is congruent
                isCongruent = rand < congruentProb;                
                if isCongruent
                    if obj.direction == obj.left 
                        centerImg = obj.imgLeft;
                    elseif obj.direction == obj.right
                        centerImg = obj.imgRight;
                    end
                    flanker = centerImg;
                else
                    if obj.direction == obj.left
                        centerImg = obj.imgLeft;
                        flanker = obj.imgRight;
                    else
                        centerImg = obj.imgRight;
                        flanker = obj.imgLeft;
                    end
                end
                obj.stmMatrix = cat(2,cat(2,flanker,centerImg),flanker);
                R = obj.stmMatrix(:,:,1);
                G = obj.stmMatrix(:,:,2);
                B = obj.stmMatrix(:,:,3);
                obj.stmMatrix(:) = 0;
            else
                R = obj.stmMatrix(:,:,1);
                G = obj.stmMatrix(:,:,2);
                B = obj.stmMatrix(:,:,3);
                isCongruent = false;
            end
            direction = obj.direction;
            obj.state = ~obj.state;
        end
        
        function [sz1, sz2, sz3, sz4, sz5] = getOutputSizeImpl(~)
            sz1 = [663 868*3];
            sz2 = [663 868*3];
            sz3 = [663 868*3];
            sz4 = [1 1];
            sz5 = [1 1];
        end
        function [fz1, fz2, fz3, fz4, fz5] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
            fz5 = true;
        end
        function [dt1, dt2, dt3, dt4, dt5] = getOutputDataTypeImpl(~)
            dt1 = 'uint8';
            dt2 = 'uint8';
            dt3 = 'uint8';
            dt4 = 'logical';
            dt5 = 'double';
        end
        function [cp1, cp2, cp3, cp4, cp5] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
            cp4 = false;
            cp5 = false;
        end
    end
end
