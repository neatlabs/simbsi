function EEG = pop_readSmarting(filename)

fid = fopen(filename,'r');
c = onCleanup(@()fclose(fid));

EEG = eeg_emptyset;
EEG.srate = str2double(fgetl(fid));
EEG.nbchan = str2double(fgetl(fid));

EEG.chanlocs = repmat(struct('labels',[],'type',[],'X',[],'Y',[],'Z',[],'radius',[],'theta',[]),EEG.nbchan,1);
for i=1:EEG.nbchan
    EEG.chanlocs(i).labels = fgetl(fid);
    EEG.chanlocs(i).type = 'EEG';
end

tmp = fscanf(fid,'%f\n');
tmp = reshape(tmp,EEG.nbchan+1,[]);
EEG.data = tmp(1:EEG.nbchan,:);

% tmp = fread(fid,[EEG.nbchan+1 inf],'float','b');
% EEG.data = tmp(1:end-1,:);
ind = find(tmp(end,:));
ev = tmp(end,ind);
EEG.event = repmat(struct('type',[],'latency',[],'urevent',[]),length(ev),1);
for i=1:length(ev)
    EEG.event(i).latency = ind(i);
    EEG.event(i).type = num2str(ev(i));
    EEG.event(i).urevent = i;
end
EEG.pnts = size(EEG.data,2);
EEG.times = (0:EEG.pnts-1)*1000/EEG.srate;
EEG.xmin  = EEG.times(1);
EEG.xmax  = EEG.times(end);
EEG.trials = 1;

% 
% fid = fopen('/data/Bin/FaceOff1.bin','r');
% x = fread(fid,[25 inf],'float','b');
% fclose(fid);
% 
% size(x)
% % size(x,1)/13496
% %x = reshape(x, [length(x)/25 25]);
% subplot(211);plot(x(1:24,:)');grid on
% subplot(212);plot(x(25,:));grid on