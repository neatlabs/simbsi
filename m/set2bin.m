function [binFilename,EEG] = set2bin(filename)
[p,n,ex] = fileparts(filename);
if strcmpi(ex,'.set')
    EEG = pop_loadset(filename);
elseif strcmpi(ex,'.bdf') || strcmpi(ex,'.edf') || strcmpi(ex,'.gdf')
    EEG = pop_biosig(filename);
elseif strcmpi(ex,'.xdf')
    EEG = pop_loadxdf(filename);
else
    error(['Cannot load file format ' ex]);
end

data = double(reshape(EEG.data(:,:),EEG.nbchan,[]));
dim = size(data);
if ~isempty(EEG.event)
    uniqueEvents = unique({EEG.event.type},'stable');
    Ne = length(uniqueEvents);
    uniqueEventsNumeric = str2double(uniqueEvents);
    if any(isnan(uniqueEventsNumeric))
        uniqueEventsNumeric = 1:Ne;
    end
    event = zeros(Ne,dim(2));
    latency = round([EEG.event.latency]);
    for k=1:Ne
        ind = ismember({EEG.event.type}, uniqueEvents{k});
        event(k,latency(ind)) = uniqueEventsNumeric(k);
    end
else
    event = zeros(1,dim(2));
end
binFilename = fullfile(p,[n, '.bin']);
fid = fopen(binFilename, 'w');
DATA = [data' sum(event',2)]; %#ok
fwrite(fid,EEG.srate,'float');
fwrite(fid,size(DATA),'int');
fwrite(fid,DATA(:),'float');
fclose(fid);