function [ALLEEG,EEG,CURRENTSET] = pop_newSimBSIEEG(ALLEEG)
% Creates a new Simulink model and sets it to stream EEG data

[FileName,PathName,FilterIndex] = uigetfile({'*.set' 'EEGLAB (.set)'; '*.bdf' 'Biosig (.bdf)';'*.xdf' 'LSL (.xdf)';'*.xdf' 'Smarting Greentek (.xdf)'},'Select file');
if FilterIndex~=0
    filename = fullfile(PathName,FileName);
else
    return;
end
[p,n,ex] = fileparts(filename);
if strcmpi(ex,'.set')
    EEG = pop_loadset(filename);
elseif strcmpi(ex,'.bdf') || strcmpi(ex,'.edf') || strcmpi(ex,'.gdf')
    EEG = pop_biosig(filename);
elseif strcmpi(ex,'.xdf')
    EEG = pop_loadxdf(filename);
else
    error(['Cannot load file format ' ex]);
end
if FilterIndex==4
    EEG = smarting2greentek(EEG);
end
    
data = double(reshape(EEG.data(:,:),EEG.nbchan,[]));
event = event2signal(EEG);

fbin = fullfile(p,[n, '.bin']);
fid = fopen(fbin, 'w');
DATA = [data' event];
fwrite(fid,EEG.srate,'float');
fwrite(fid,size(DATA),'int');
fwrite(fid,DATA(:),'float');
fclose(fid);

val = '';
if ~isempty(EEG.chanlocs)
    for k=1:EEG.nbchan
        val = [val ''',''' EEG.chanlocs(k).labels];
    end
else
    for k=1:EEG.nbchan
        val = [val ''',''' num2str(k)];
    end
end
val(end+1) = '''';
val([1 2]) = [];
val = ['{' val '}'];


sys = 'SimBSI_data_source';
if exist(sys,'file')
    in = inputdlg('System name','',1,{sys});
    if isempty(in)
        delete(fbin);
        return;
    end
    sys = in{1};
end
new_system(sys) % Create the model
open_system(sys) % Open the model

cs = getActiveConfigSet(sys);
cs.set_param('StartTime','0');
cs.set_param('StopTime','inf');
cs.set_param('SolverType','fixed-step');
cs.set_param('FixedStep',num2str(1/EEG.srate));
cs.set_param('SaveState','off');
cs.set_param('SaveTime','off');

add_block('SimBSI/Sources/From EEGLAB',[sys '/From File'],'Position',[35 55 95 115]);

add_block('SimBSI/Sinks/LSLOutlet',[sys '/EEG2LSL'],'Position',[230 21 290 79]);
add_block('built-in/Ground',[sys '/Ground'],'Position',[185 55 205 75]);
add_block('SimBSI/Sinks/LSLOutlet',[sys '/Events2LSL'],'Position',[230 111 290 169]);
add_block('built-in/Ground',[sys '/Ground1'],'Position',[185 145 205 165]);
add_block('built-in/Scope',[sys '/Scope'],'Position',[245 201 275 234]);
scopeConfObj = get_param([sys '/Scope'],'ScopeConfiguration');
scopeConfObj.NumInputPorts = '2';
scopeConfObj.LayoutDimensions = [2 1];

add_line(sys,'From File/1','Scope/1','autorouting','on')
add_line(sys,'From File/2','Scope/2','autorouting','on')
add_line(sys,'From File/1','EEG2LSL/1','autorouting','on')
add_line(sys,'Ground/1','EEG2LSL/2','autorouting','on')
add_line(sys,'From File/2','Events2LSL/1','autorouting','on')
add_line(sys,'Ground1/1','Events2LSL/2','autorouting','on')

set_param([sys '/From File'],'filename',filename);

set_param([sys '/EEG2LSL'],'streamName','SimEEG');
set_param([sys '/EEG2LSL'],'streamType','EEG');
set_param([sys '/EEG2LSL'],'numberOfChannels',num2str(EEG.nbchan));
set_param([sys '/EEG2LSL'],'channelLabels',val);
set_param([sys '/EEG2LSL'],'samplingRate',num2str(EEG.srate));

set_param([sys '/Events2LSL'],'streamName','SimMarkers');
set_param([sys '/Events2LSL'],'streamType','Markers');
set_param([sys '/Events2LSL'],'numberOfChannels','1');
set_param([sys '/Events2LSL'],'channelLabels','mrk');
set_param([sys '/Events2LSL'],'samplingRate','0');

set_param(sys,'CloseFcn','(fbin)delete');
[ALLEEG,EEG,CURRENTSET] = eeg_store(ALLEEG,EEG);
eeglab redraw;
end