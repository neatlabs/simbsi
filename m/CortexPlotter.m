classdef CortexPlotter < matlab.System
    % CortexPlotter Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
   properties
        clim = [-1 1];      % Voltage limits
    end
    
    properties(Nontunable)
        figureTitle = ' ';  % Figure title
    end

    % Pre-computed constants
    properties(Access = private)
        hm;
        fig;
        ax;
        hPatch;
        enable = true;
        leftH;
        rightH;
        leftRightView = 0;
    end

    methods(Access = protected)
        
        function processTunedPropertiesImpl(obj)
            if ~obj.enable
                return;
            end
            obj.ax.CLim = obj.clim;
            obj.fig.Name = obj.figureTitle;
        end
        
        function setupImpl(obj)
            obj.fig = figure('Name',obj.figureTitle, 'MenuBar','none','ToolBar','figure','Visible','off');
            obj.fig.Position(3:4) = [500 350];
            obj.fig.Visible = 'on';
            obj.ax = axes(obj.fig,'CameraViewAngle',5);
            obj.hm = evalin('base','hm');
            
            path = fileparts(which('headModel.m'));
            path = fullfile(path,'+vis','icons');
            lrBrain = imresize(imread([path filesep 'LeftRightView.png']),[25 28]);
            [~,~, obj.leftH, obj.rightH] = geometricTools.splitBrainHemispheres(obj.hm.cortex);

            toolbarHandle = findall(obj.fig,'Type','uitoolbar');
            hcb1 = uitoggletool(toolbarHandle,'CData',lrBrain,'Separator','off','HandleVisibility','off','TooltipString','View left/right hemisphere','userData',[],'State','off');
            set(hcb1,'OnCallback',@(src,event)viewHemisphere(obj,hcb1),'OffCallback',@(src, event)viewHemisphere(obj,hcb1));
        end
        
        function viewHemisphere(obj,~)
            obj.leftRightView = obj.leftRightView+1;
            val = get(obj.hPatch,'FaceVertexCData');
            switch obj.leftRightView
                case 1
                    val(obj.rightH) = nan;
                case 2
                    val(obj.leftH) = nan;
                otherwise
                    obj.leftRightView = 0;
            end
            set(obj.hPatch,'FaceVertexCData',val);
        end

        function stepImpl(obj,x)
            x = x';
             if isempty(obj.hPatch)
                obj.hPatch = patch('vertices',obj.hm.cortex.vertices,'faces',obj.hm.cortex.faces,'FaceVertexCData',x',...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',1,'SpecularColorReflectance',0,...
                    'SpecularExponent',25,'SpecularStrength',0.1,'parent',obj.ax);
                axis(obj.ax,'off')
                axis(obj.ax,'equal');
                axis(obj.ax,'vis3d');
                camlight(obj.ax,'headlight');
                camlight(obj.ax,90,0);
                camlight(obj.ax,-90,0);
                view(obj.ax,[127 36]);
                colormap(obj.ax, bipolar(256,0.7));
                obj.ax.CLim = obj.clim;
                cb = colorbar(obj.ax);
                cb.Label.String = 'PCD';
                rotate3d(obj.ax);
                drawnow;
            else
                try %#ok
                    switch obj.leftRightView
                        case 1
                            x(obj.rightH) = nan;
                        case 2
                            x(obj.leftH) = nan;
                    end
                    set(obj.hPatch,'FaceVertexCData',x);
                end
            end
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
        function releaseImpl(obj)
            if ~obj.enable
                return;
            end
            close(obj.fig);
        end
    end
end
