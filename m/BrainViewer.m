classdef BrainViewer < matlab.System & matlab.system.mixin.CustomIcon
    % BrainViewer Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
   properties
       scalpClim = [-10 10];     % Scalp scale
       cortexClim = [-1 1];      % Cortex scale
       autoscale = false;        % Autoscale
       colorMap = 'human';    % Colormap
   end
   
   properties(Nontunable)
       figTitle = 'EEG source image'; % Figure title
   end
    
   properties(Hidden)
       colorMapSet = matlab.system.StringSet({'human','bipolar','jet','hsv','parula'});
   end

    % Pre-computed constants
    properties(Access = private)
        hm;
        fig;
        hLabels;
        hSensors;
        hScalp;
        hCortexL;
        hCortexR
        hAxes;
        hText;
        enable = true;
        showSensors = true;
        showLabels = false;
        showEvent = false;
        leftH;
        rightH;
        leftRightView = 0;
        colorBar;
        scalpAlpha;
        cortexAlpha;
        indz;
        W
    end

    methods(Access = protected)
        function icon = getIconImpl(~)
            rootDir = fileparts(which('SimBSI'));
            icon = matlab.system.display.Icon(fullfile(rootDir,'resources','icons','brain_viewer.png'));
        end
        function processTunedPropertiesImpl(obj)
            if ~isvalid(obj.fig)
                return;
            end
            cmap = obj.getCMap();
            if ~obj.autoscale
                obj.hAxes.CLim = obj.scalpClim;
                if obj.hAxes.CLim(1) < 0
                    colormap(obj.hAxes, cmap);
                else
                    colormap(obj.hAxes, cmap(end/2:end,:));
                end
            else
                if obj.hAxes.CLim(1) < 0
                    colormap(obj.hAxes, cmap);
                    obj.hAxes.CLim = [-1 1];
                else
                    colormap(obj.hAxes, cmap(end/2:end,:));
                    obj.hAxes.CLim = [0 1];
                end
            end
            if obj.colorBar.Ticks(1) == 0
                obj.colorBar.Ticks = obj.colorBar.Ticks([1 end]);
                obj.colorBar.TickLabels = {'0', 'Max'};
            else
                obj.colorBar.Ticks = obj.colorBar.Ticks([1 find(obj.colorBar.Ticks == 0) end]);
                obj.colorBar.TickLabels = {'Min' '0', 'Max'};
            end
            obj.fig.Name = obj.figTitle;
        end
        
        function setupImpl(obj)
            obj.hm = evalin('base','hm');
            obj.indz = obj.hm.scalp.vertices(:,3) < min(obj.hm.channelSpace(:,3)) - 0.1*abs(min(obj.hm.channelSpace(:,3)));
            [fvL,fvR, obj.leftH, obj.rightH] = geometricTools.splitBrainHemispheres(obj.hm.cortex);
            obj.W = geometricTools.localGaussianInterpolator(obj.hm.channelSpace,obj.hm.scalp.vertices,0.03,true);
            
            figHandlers = findobj('Tag','BrainViewer');
            if ~isempty(figHandlers)
                for k=1:length(figHandlers)
                    if strcmp(figHandlers(k).UserData,obj.figTitle)
                        obj.fig = figHandlers(k);
                        obj.hAxes = findobj(obj.fig,'Type','Axes');
                        obj.colorBar = findobj(obj.fig,'Type','ColorBar');
                        obj.scalpAlpha = findobj(obj.fig,'Tag', 'scalpAlpha');
                        obj.cortexAlpha = findobj(obj.fig,'Tag', 'cortexAlpha');
                        obj.hLabels = flipud(findobj(obj.hAxes.Children,'Type','Text'));
                        obj.hSensors = findobj(obj.hAxes.Children,'Type','Scatter');
                        obj.hCortexL = findobj(obj.hAxes.Children,'Tag','CortexL');
                        obj.hCortexR = findobj(obj.hAxes.Children,'Tag','CortexR');
                        obj.hScalp = findobj(obj.hAxes.Children,'Tag','scalp');
                        break;
                    end
                end
            end
            
            if isempty(obj.fig)
                obj.fig = figure('Name',obj.figTitle, 'ToolBar','figure','Visible','off','UserData',obj.figTitle,'Tag','BrainViewer');
                %obj.fig.Position(3:4) = [500 350];
                position = get(obj.fig,'Position');
                set(obj.fig,'Position',[position(1:2) 597   525]);
                obj.fig.Visible = 'on';
                obj.hAxes = axes(obj.fig,'CameraViewAngle',8,'Position',[0.0386    0.1424    0.9586    0.7910]);
                
                path = fileparts(which('headModel.m'));
                path = fullfile(path,'+vis','icons');
                labelsOn  = imresize(imread([path filesep 'labelsOn.png']),[28 28]);
                sensorsOn = imresize(imread([path filesep 'sensorsOn.png']),[20 20]);
                lrBrain = imresize(imread([path filesep 'LeftRightView.png']),[25 28]);
                
                toolbarHandle = findall(obj.fig,'Type','uitoolbar');
                hcb(1) = uitoggletool(toolbarHandle,'CData',labelsOn,'Separator','off','HandleVisibility','off','TooltipString','Labels On/Off','State','off');
                set(hcb(1),'OnCallback',@(src,event)showLabelsBtn(obj,hcb(1)),'OffCallback',@(src, event)showLabelsBtn(obj,hcb(1)));
                
                hcb(2) = uitoggletool(toolbarHandle,'CData',sensorsOn,'Separator','off','HandleVisibility','off','TooltipString','Sensors On/Off','State','off');
                set(hcb(2),'OnCallback',@(src,event)showSensorsBtn(obj,hcb(2)),'OffCallback',@(src, event)showSensorsBtn(obj,hcb(2)));
                
                hcb(3) = uitoggletool(toolbarHandle,'CData',lrBrain,'Separator','off','HandleVisibility','off','TooltipString','View left/right hemisphere','State','off');
                set(hcb(3),'OnCallback',@(src,event)viewHemisphereBtn(obj,hcb(3)),'OffCallback',@(src, event)viewHemisphereBtn(obj,hcb(3)));
                
                pn = uipanel(obj.fig,'Title','Transparency','FontWeight','bold');
                pn.Position = [0 0.0 1 0.12];
                obj.cortexAlpha = uicontrol(pn,'Style', 'slider','Min',0,'Max',1,'Value',1,'Units','normalized',...
                    'Position',[0.2751, 0.1000, 0.1954, 0.3922],'Callback',@obj.setCortexAlpha,'Tag','cortexAlpha');
                addlistener(obj.cortexAlpha,'ContinuousValueChange',@obj.setCortexAlpha);
                uicontrol(pn,'Style', 'text','String','Cortex','Units','normalized',...
                    'Position',[0.2665, 0.5294, 0.2112, 0.2941]);
                
                obj.scalpAlpha = uicontrol(pn,'Style', 'slider','Min',0,'Max',1,'Value',0.25,'Units','normalized',...
                    'Position',[0.5050, 0.1000, 0.1954, 0.3922], 'Callback',@obj.setScalpAlpha,'Tag','scalpAlpha');
                addlistener(obj.scalpAlpha,'ContinuousValueChange',@obj.setScalpAlpha);
                uicontrol(pn,'Style', 'text','String','Scalp','Units','normalized',...
                    'Position',[0.5065, 0.5294, 0.1867, 0.2941]);
                
                N = length(obj.hm.labels);
                k = 1.1;
                obj.hLabels = zeros(N,1);
                for it=1:N
                    obj.hLabels(it) = text(...
                        'Position',k*obj.hm.channelSpace(it,:),...
                        'String',obj.hm.labels{it},...
                        'Parent',obj.hAxes);
                end
                set(obj.hLabels,'Visible','off');
                hold(obj.hAxes,'on');
                obj.hSensors = scatter3(obj.hAxes,obj.hm.channelSpace(:,1),obj.hm.channelSpace(:,2),...
                    obj.hm.channelSpace(:,3),'filled','MarkerEdgeColor','k','MarkerFaceColor','y');
                if ~obj.showSensors
                    set(obj.hSensors,'Visible','off');
                end
                
                skinColor = [1,.75,.65];
                obj.hCortexL = patch('vertices',fvL.vertices,'faces',fvL.faces,'FaceColor',skinColor,...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',1,...
                    'SpecularColorReflectance',0,'SpecularExponent',25,'SpecularStrength',0.1,'parent',obj.hAxes,'Tag','CortexL');
                
                obj.hCortexR = patch('vertices',fvR.vertices,'faces',fvR.faces,'FaceColor',skinColor,...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',1,...
                    'SpecularColorReflectance',0,'SpecularExponent',25,'SpecularStrength',0.1,'parent',obj.hAxes,'Tag','CortexR');
                
                
                obj.hScalp = patch('vertices',obj.hm.scalp.vertices,'faces',obj.hm.scalp.faces,'FaceColor',skinColor,...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',obj.scalpAlpha.Value,...
                    'SpecularColorReflectance',0,'SpecularExponent',25,'SpecularStrength',0.25,'Parent',obj.hAxes,'Tag','scalp');
                axis(obj.hAxes,'off')
                axis(obj.hAxes,'equal');
                axis(obj.hAxes,'vis3d');
                axis(obj.hAxes,'tight');
                camlight(obj.hAxes,'headlight');
                camlight(obj.hAxes,90,0);
                camlight(obj.hAxes,-90,0);
                view(obj.hAxes,[127 36]);
                cmap = obj.getCMap();
                if obj.hAxes.CLim(1) < 0
                    colormap(obj.hAxes, cmap);
                else
                    colormap(obj.hAxes, cmap(end/2:end,:));
                end
                obj.hAxes.CLim = obj.scalpClim;
                obj.colorBar = colorbar(obj.hAxes,'Position',[0.8749 0.2625 0.0252 0.5464]);
                rotate3d(obj.hAxes);
                drawnow;
            end
            obj.processTunedPropertiesImpl();
        end

        function stepImpl(obj, y, x)
            try %#ok
                if obj.scalpAlpha.Value > 0.02
                    % F = scatteredInterpolant(obj.hm.channelSpace,y','natural','linear');
                    % y = F(obj.hm.scalp.vertices);
                    y = obj.W*y';
                    y(obj.indz) = 0;
                    if obj.autoscale
                        y = y/(max(abs(y))+eps);
                    end
                    set(obj.hScalp,'FaceVertexCData',mean(y,2));
                end
                if obj.cortexAlpha.Value > 0.02 && obj.scalpAlpha.Value < 0.95
                    x = x';
                    if obj.autoscale
                        x = x/(max(abs(x))+eps);
                    else
                        x = x*obj.cortexClim(2);
                    end
                    set(obj.hCortexL,'FaceVertexCData',mean(x(obj.leftH),2));
                    set(obj.hCortexR,'FaceVertexCData',mean(x(obj.rightH),2));
%                     switch obj.leftRightView
%                         case 1
%                             x(obj.rightH) = nan;
%                         case 2
%                             x(obj.leftH) = nan;
%                     end
%                     set(obj.hCortex,'FaceVertexCData',x);
                end
            end
        end
        
        function viewHemisphereBtn(obj,~)
            obj.leftRightView = obj.leftRightView+1;
            %val = get(obj.hCortex,'FaceVertexCData');
            switch obj.leftRightView
                case 1
                    set(obj.hCortexL,'Visible','off')
                    set(obj.hCortexR,'Visible','on')
                case 2
                    set(obj.hCortexL,'Visible','on')
                    set(obj.hCortexR,'Visible','off')
                otherwise
                    set(obj.hCortexL,'Visible','on')
                    set(obj.hCortexR,'Visible','on')
                    obj.leftRightView = 0;
            end
            %disp(obj.leftRightView)
            %set(obj.hCortex,'FaceVertexCData',val);
        end
        
        function showLabelsBtn(obj, ~)
            if ~obj.enable
                return;
            end
            obj.showLabels = ~obj.showLabels;
            if obj.showLabels
                set(obj.hLabels,'Visible','on');
            else
                set(obj.hLabels,'Visible','off');
            end
        end
        function showSensorsBtn(obj, ~)
            if ~obj.enable
                return;
            end
            obj.showSensors = ~obj.showSensors;
            if obj.showSensors
                set(obj.hSensors,'Visible','on');
            else
                set(obj.hSensors,'Visible','off');
            end
        end
        function setCortexAlpha(obj,~,~)
            obj.hCortex.FaceAlpha = obj.cortexAlpha.Value;
        end
        function setScalpAlpha(obj,~,~)
            obj.hScalp.FaceAlpha = obj.scalpAlpha.Value;
        end
        
        function cmap = getCMap(obj)
            if strcmp(obj.colorMap,'bipolar')
                cmap = bipolar(256,0.7);
            elseif strcmp(obj.colorMap,'human')
                cmap = vis.human(256);
            elseif strcmp(obj.colorMap,'jet')
                cmap = jet(256);
            elseif strcmp(obj.colorMap,'hsv')
                cmap = hsv(256);
            elseif strcmp(obj.colorMap,'parula')
                cmap = parula(256);
            else
                cmap = vis.human(256);
            end
        end
    end
end
