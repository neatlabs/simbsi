classdef MultichannelScope < matlab.System
    % MultichannelScope Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        figYLabel = {'Ch1'};            % Y-label
        samplingRate = 1000;            % Sampling rate (Hz)
        spread = 10;                    % Spread
        pageLength = 2;                 % Page length (sec)
    end

    properties(Nontunable)
        figTitle = 'Multichannel data'; % Title
    end

    % Pre-computed constants
    properties(Access = private)
        fig = [];
        ax = [];
        hLines
        buffer;
        n = 1;                          % Number of channels
        yticks;
        ytickslabel;
    end

    methods(Access = protected)
        
        function processTunedPropertiesImpl(obj)
            if ~isvalid(obj.fig)
                return;
            end
            obj.hLines = [];
            obj.yticks = fliplr((0:obj.n-1)*obj.spread);
            [~,loc] = sort(obj.yticks);
            set(obj.ax,'YTick',obj.yticks(loc),'YTickLabel',obj.ytickslabel(loc));
            set(obj.ax,'YLim',[-obj.spread obj.yticks(1)+obj.spread]);
            title(obj.ax,obj.figTitle,'Color','w');
        end
        
        function setupImpl(obj)
            figHandlers = findobj('Tag','MultichannelScope');
            if ~isempty(figHandlers)
                for k=1:length(figHandlers)
                    if strcmp(figHandlers(k).UserData,obj.figTitle)
                        obj.fig = figHandlers(k);
                        obj.ax = findobj(obj.fig,'Type','axes');
                        obj.ax.UserData = obj;
                        obj.hLines = findall(obj.ax,'Type','Line');
                        break;
                    end
                end
            end
            if isempty(obj.fig)
                obj.fig = figure('Color',0.15*ones(1,3),'Tag','MultichannelScope','KeyPressFcn',@keyPress);
                obj.ax = axes();
            end
            obj.fig.UserData = obj.figTitle;
            obj.ax.UserData = obj;
        end
        

        function stepImpl(obj,y)
            nt = size(y,1);
            if isempty(obj.buffer)
                obj.n = size(y,2);
                obj.buffer = zeros(round(obj.pageLength*obj.samplingRate),obj.n);
            end
            if isempty(obj.hLines)
                obj.yticks = fliplr((0:obj.n-1)*obj.spread);
                obj.setYTicksLabel();
            else
                obj.yticks = sort(get(obj.ax,'YTick'),'descend');
            end
            y = bsxfun(@plus,y,obj.yticks);
            obj.buffer = circshift(obj.buffer,-nt);
            obj.buffer(end-nt+1:end,:) = y;
            if isempty(obj.hLines)
                cla(obj.ax);
                t = 0:1/obj.samplingRate:obj.pageLength-1/obj.samplingRate;
                obj.hLines = plot(obj.ax,t,obj.buffer);
                title(obj.ax,obj.figTitle,'Color','w');
                xlabel(obj.ax,'Time (sec)');
                set(obj.ax,'Color','k','XColor','w','YColor','w');
                [~,loc] = sort(obj.yticks);
                set(obj.ax,'YTick',obj.yticks(loc),'YTickLabel',obj.ytickslabel(loc));
                set(obj.ax,'YLim',[-obj.spread obj.yticks(1)+obj.spread]);
                grid(obj.ax,'on');
                obj.ax.UserData = obj;
            else
                for k=1:obj.n
                    set(obj.hLines(k),'YData',obj.buffer(:,k));
                end
            end
        end
        
        function setYTicksLabel(obj)
            if isempty(obj.figYLabel)
                obj.ytickslabel = cell(1,obj.n);
                for k=1:obj.n
                    obj.ytickslabel{k} = num2str(k);
                end
            else
                obj.ytickslabel = obj.figYLabel;
            end
        end
        

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
    end
end

function keyPress(fig, key, evnt)
ax = findobj(fig,'Type','axes');
obj = ax.UserData;
if length(obj.yticks) > 1 && key.Character == '+'    
    obj.yticks = obj.yticks*2;
    yl = [-obj.yticks(end-1) obj.yticks(1)+obj.yticks(end-1)];
elseif length(obj.yticks) > 1 && key.Character == '-'
    obj.yticks = obj.yticks/2;
    yl = [-obj.yticks(end-1) obj.yticks(1)+obj.yticks(end-1)];
elseif length(obj.yticks) == 1 && key.Character == '+'    
    yl = ax.YLim*2;
elseif length(obj.yticks) == 1 && key.Character == '-'
    yl = ax.YLim/2;
else
    return
end
[~,loc] = sort(obj.yticks);
set(obj.ax,'YTick',obj.yticks(loc),'YLim',yl);
end