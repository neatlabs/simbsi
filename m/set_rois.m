function set_rois(src,~)
hBlock = src.UserData{1};
hViewer = src.UserData{2};
if isempty(hViewer.roiLabels)
    return;
end
lab = '{''';
for k=1:length(hViewer.roiLabels)
    lab = [lab hViewer.roiLabels{k} ''','''];
end
lab(end) = [];
lab(end) = '}';
set_param(hBlock,'roi',lab);
end