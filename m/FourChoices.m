classdef FourChoices < matlab.System & matlab.system.mixin.Propagates
    % FourChoices Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        targetProb    = 0.8; % Target probability
        ImgTarget     = zeros([640 580 3],'uint8');
        ImgNontarget  = zeros([640 580 3],'uint8');
        locationIndex = zeros([640 580*5 3],'uint8');
        seed = 0;
    end

    % Pre-computed constants
    properties(Access = private)
        state = true;
        locationSpace = [1 2 4 5];
        stmMatrix = zeros([640 580*5 3],'uint8');
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            rng(obj.seed,'twister');
        end

        function [R,G,B, target, loc] = stepImpl(obj)
            if obj.state
                loc = obj.locationSpace(randperm(4,1));
                target = rand<obj.targetProb;
                if target
                    obj.stmMatrix(obj.locationIndex==loc) = obj.ImgTarget;
                else
                    obj.stmMatrix(obj.locationIndex==loc) = obj.ImgNontarget;
                end
                R = obj.stmMatrix(:,:,1);
                G = obj.stmMatrix(:,:,2);
                B = obj.stmMatrix(:,:,3);
                obj.stmMatrix(obj.locationIndex==loc) = 0;
            else
                R = obj.stmMatrix(:,:,1);
                G = obj.stmMatrix(:,:,2);
                B = obj.stmMatrix(:,:,3);
                target = false;
                loc = 0;
            end
            obj.state = ~obj.state;
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
        end
        
        function [sz1, sz2, sz3, sz4, sz5] = getOutputSizeImpl(~)
            sz1 = [640 2900];
            sz2 = [640 2900];
            sz3 = [640 2900];
            sz4 = [1 1];
            sz5 = [1 1];
        end
        function [fz1, fz2, fz3, fz4, fz5] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
            fz5 = true;
        end
        function [dt1, dt2, dt3, dt4, dt5] = getOutputDataTypeImpl(~)
            dt1 = 'uint8';
            dt2 = 'uint8';
            dt3 = 'uint8';
            dt4 = 'logical';
            dt5 = 'double';
        end
        function [cp1, cp2, cp3, cp4, cp5] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
            cp4 = false;
            cp5 = false;
        end
    end
end
