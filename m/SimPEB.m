classdef SimPEB < matlab.System & matlab.system.mixin.Propagates
    % Simulink block implementation of the PEB+ algorithm.

    % Public, tunable properties
    properties
        maxIter    = 100;   % Maximum number of iterations
        verbose    = false; % Produce per-iteration prints
        maxTol     = 1e-1;  % Maximum tolerance of logE change
        bufferSize = 100;   % History buffer size
        gammaMin   = 1;     % Minimum gamma allowed in the first stage
        doPruning  = true;  % Enable SBL stage
    end
    
    properties(Nontunable)
        H 
        Delta
        blocks
        indG
        indV
        lambdaBufferSize = 100; % Lambda buffer size
    end
    
    properties(DiscreteState)
    end

    % Pre-computed constants
    properties(Access = private)
        Ny
        Nx
        Ng
        Hi
        HiHit
        CiHt
        Ut
        s2
        Tx
        Iy
        lambdaBuffer = [];
        smoothLambda = true;
        overlaping = 5;
        tape
        X0
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            obj.lambdaBuffer = nan(obj.lambdaBufferSize,1);
            [obj.Ny,obj.Nx] = size(obj.H);
            obj.Ng = size(obj.blocks,2);
            
            obj.tape = hanning(2*obj.overlaping)';
            obj.X0 = zeros(obj.Nx,obj.overlaping);
            obj.Hi    = cell(1,obj.Ng);
            obj.HiHit = zeros([obj.Ny, obj.Ny, obj.Ng]);
            Ci        = sparse(obj.Nx^2,obj.Ng);
            obj.CiHt  = sparse(obj.Nx*obj.Ny, obj.Ng);
            ind1      = zeros(obj.Nx,obj.Nx);
            ind2      = zeros(obj.Nx,obj.Ny);
            obj.Iy    = eye(obj.Ny);
            
            for k=1:obj.Ng
                
                % Per-block square root precision matrix
                Di = obj.Delta(obj.blocks(:,k),obj.blocks(:,k));
                
                % Per-block covariance matrix
                sqCi_k = inv(Di);
                sqCi_k = sqCi_k/norm(sqCi_k,'fro');
                ind1(obj.blocks(:,k),obj.blocks(:,k)) = 1;
                ind2(obj.blocks(:,k),:) = 1;
                C_i = sqCi_k*sqCi_k';
                Ci(ind1==1,k) = C_i(:);
                CiHt_k = C_i*obj.H(:,obj.blocks(:,k))';
                obj.CiHt(ind2==1,k) = CiHt_k(:);
                ind2(obj.blocks(:,k),:) = 0;
                ind1(obj.blocks(:,k),obj.blocks(:,k)) = 0;
                
                % Per-block standardized gain matrices
                Hi_k = obj.H(:,obj.blocks(:,k))*sqCi_k;
                obj.Hi{k} = Hi_k;
                obj.HiHit(:,:,k) = Hi_k*Hi_k';
            end
            
            % Unweighted prior covariance
            C = reshape(sum(Ci,2),[obj.Nx, obj.Nx]);
            
            % Fix possible 0 diagonal elements
            dc = diag(C);
            dc(dc==0) = median(dc(dc~=0));
            C = C - diag(diag(C)) + diag(dc);
            
            % Compute svd
            sqC = chol(C);
            [U,s] = svd(obj.H*sqC,'econ');
            obj.s2 = diag(s).^2;
            obj.Ut = U';
        end
        
        %%
        function [X, lambda, gamma, logE] = stepImpl(obj,Y)
            if obj.Ny ~= size(Y,1)
                Y = Y';
            end
            if all(Y(:)==0)
                X = zeros(obj.Nx,size(Y,2));
                lambda = 0;
                gamma = ones(obj.Ng,1);
                logE = -inf;
            else
                [lambda, gamma, logE] = learning(obj,Y);
                X = inference(obj, Y);
            end
%             X(:,1:obj.overlaping) = (obj.X0+X(:,1:obj.overlaping))/2;
%             Xtmp = bsxfun(@times,[obj.X0 X(:,1:obj.overlaping)],obj.tape);
%             obj.X0 = X(:,end-obj.overlaping+1:end);
%             X = [Xtmp X(:,obj.overlaping+1:end-obj.overlaping)];
            X = X';
        end
        
        function [lambda, gamma, logE] = learning(obj,Y)
            [lambda0, gamma0] = initHyperparameters(obj, Y);
            [lambda, gamma, ~, history] = optimizeFullModel(obj,Y,lambda0, gamma0);
            if obj.doPruning
                [gamma,history] = pruning(obj,Y,lambda,gamma,history);
            end
            logE = history.logE(history.pointer);
            [~, iSy] = obj.calculateModelCov(lambda,gamma);
            SxHt = reshape(obj.CiHt*gamma,[obj.Nx obj.Ny]);
            obj.Tx = SxHt*iSy;
        end
        function X = inference(obj, Y)
            X = obj.Tx*Y;
        end
        
        function [lambda0, gamma0] = initHyperparameters(obj, Y)
            UtY2 = (obj.Ut*Y).^2;
            S = [obj.s2 obj.s2*0+1];
            phi = abs(mean((S'*S)\(S'*UtY2),2));
            gamma0  = phi(1);
            lambda0 = phi(2);
        end
        
        function [lambda, gamma, gamma_F, history] = optimizeFullModel(obj,Y,lambda0, gamma0)
            UtY2 = (obj.Ut*Y).^2;
            Nt = size(Y,2);
            Cy = Y*Y'/Nt;
            gamma = ones(obj.Ng,1);
            lambda = lambda0;
            gamma_F = gamma0;
            gamma(:) = gamma_F;
            
            history = struct('lambda',nan(obj.bufferSize,1),'gamma_F',nan(obj.bufferSize,1),'logE',nan(obj.bufferSize,1),'pointer',1);
            history.lambda(1)  = lambda;
            history.gamma_F(1) = gamma_F;
            history.logE(1)    = calculateLogEvidence(obj,Cy,lambda,gamma);
            
            for k=2:obj.maxIter
                psi = gamma_F*obj.s2+lambda;
                psi2 = psi.^2;
                
                lambda   = lambda *sum(mean(bsxfun(@times,UtY2,     1./psi2),2))/(eps+sum(     1./psi));
                gamma_F  = gamma_F*sum(mean(bsxfun(@times,UtY2,obj.s2./psi2),2))/(eps+sum(obj.s2./psi));
                gamma_F(gamma_F<obj.gammaMin) = obj.gammaMin;
                gamma(:) = gamma_F;
                
                history.logE(k) = calculateLogEvidence(obj,Cy,lambda,gamma);
                
                if obj.verbose
                    fprintf('%i => diff(logE): %.4g   logE: %.5g   Lambda: %.4g   Gamma: %.4g\n',...
                        k,abs(diff(fliplr(history.logE(k-1:k)))),history.logE(k),lambda,gamma_F);
                end
                
                % Check convergence and exit condition
                if diff(history.logE(k-1:k)) < obj.maxTol, break;end
            end
            history.pointer = k;
            if obj.smoothLambda
                obj.lambdaBuffer = circshift(obj.lambdaBuffer,-1);
                obj.lambdaBuffer(end) = lambda;
                lambda = mean(obj.lambdaBuffer(~isnan(obj.lambdaBuffer)));
                obj.lambdaBuffer(end) = lambda;
            end
        end
        
        function [gamma,history] = pruning(obj,Y,lambda,gamma,history)
            Nt = size(Y,2);
            Cy = Y*Y'/Nt;
            for k=1:obj.maxIter
                [~, iSy] = obj.calculateModelCov(lambda,gamma);
                num = gamma;
                den = num;
                for i=1:obj.Ng
                    Hi_iSy = obj.Hi{i}'*iSy;
                    num(i) = norm(Hi_iSy*Y,'fro');
                    den(i) = sqrt(abs(sum(sum((Hi_iSy)'.*obj.Hi{i}))));
                end
                gamma = (gamma/sqrt(Nt)).*num./(den+eps);
                history.pointer = history.pointer+1;
                history.logE(history.pointer) = calculateLogEvidence(obj,Cy,lambda,gamma);
                if obj.verbose
                    fprintf('%i => diff(logE): %.4g   logE: %.5g   Sum Gamma: %.4g\n',history.pointer,diff(...
                        history.logE(history.pointer-1:history.pointer)),history.logE(history.pointer),sum(nonzeros(gamma)));
                end
                if diff(history.logE(history.pointer-1:history.pointer)) < obj.maxTol, break;end
            end
        end
        
        function logE = calculateLogEvidence(obj,Cy,lambda,gamma)
            [Sy, iSy] = calculateModelCov(obj,lambda,gamma);
            % Calculate logdet
            log_det = log(det(Sy));     
            if isinf(log_det)
                e = eig(S);
                e(e<0) = eps;
                log_det = sum(log(e));
            end
            logE = (-1/2)*(trace(Cy*iSy) + log_det);
        end
        
        function log_d = logDet(S)
            log_d = log(det(S));     
            if isinf(log_d)
                e = eig(S);
                e(e<0) = eps;
                log_d = sum(log(e));
            end
        end
        
        function [Sy, iSy] = calculateModelCov(obj,lambda,gamma, indices)
            if nargin < 4, indices = 1:obj.Ng;end
            gHHt = sum(bsxfun(@times, obj.HiHit(:,:,indices),permute(gamma(indices),[3 2 1])),3);
            Sy = lambda*obj.Iy+gHHt;
            try
                iSy = invChol_mex(double(Sy));
            catch ME
                warning(ME.message)
                if strcmp(ME.identifier,'MATLAB:invChol_mex:dpotrf:notposdef')
                    warning('Possibly the data is rank deficient!')
                end
                [Utmp,S,Vtmp] = svd(Sy);
                stmp = real(diag(S));
                invS = 1./stmp;
                invS(isinf(invS)) = 0;
                iSy = Utmp*diag(invS)*Vtmp';
            end
        end

        %%
        %function [sz1, sz2, sz3, sz4, sz5, sz6] = getOutputSizeImpl(obj)
        function [sz1, sz4, sz5, sz6] = getOutputSizeImpl(obj)
            sz = propagatedInputSize(obj, 1);
            sz1 = [sz(1) size(obj.H,2)];
            sz2 = [sz(1) length(obj.indV)];
            sz3 = sz;
            sz4 = 1;
            sz5 = [size(obj.blocks,2) 1];
            sz6 = 1;
        end
        
        function [fz1, fz4, fz5, fz6] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
            fz5 = true;
            fz6 = true;
        end
        
        function [dt1,dt4,dt5,dt6] = getOutputDataTypeImpl(obj)
            dt1 = propagatedInputDataType(obj, 1);
            dt2 = propagatedInputDataType(obj, 1);
            dt3 = propagatedInputDataType(obj, 1);
            dt4 = propagatedInputDataType(obj, 1);
            dt5 = propagatedInputDataType(obj, 1);
            dt6 = propagatedInputDataType(obj, 1);
        end
        
        function [cp1,cp4,cp5,cp6] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
            cp4 = false;
            cp5 = false;
            cp6 = false;
        end
        
    end
end
