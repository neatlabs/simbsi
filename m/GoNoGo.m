classdef GoNoGo < matlab.System & matlab.system.mixin.Propagates
    % FourChoices Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        goProb = 0.8; % Go probability
        stmMatrix = zeros([900 900, 3, 2],'uint8');
        seed = 0;
    end

    % Pre-computed constants
    properties(Access = private)
        blank = zeros([900 900],'uint8');
        state = true;
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            rng(obj.seed,'twister');
        end

        function [R,G,B, target] = stepImpl(obj)
            if obj.state
                if rand < obj.goProb
                    pointer = 1;
                else
                    pointer = 2;
                end
                R = obj.stmMatrix(:,:,1,pointer);
                G = obj.stmMatrix(:,:,2,pointer);
                B = obj.stmMatrix(:,:,3,pointer);
                target = pointer==1;
            else
                R = obj.blank;
                G = obj.blank;
                B = obj.blank;
                target = false;
            end
            obj.state = ~obj.state;
        end

        function [sz1, sz2, sz3, sz4] = getOutputSizeImpl(~)
            sz1 = [900 900];
            sz2 = [900 900];
            sz3 = [900 900];
            sz4 = [1 1];
        end
        function [fz1, fz2, fz3, fz4] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
        end
        function [dt1, dt2, dt3, dt4] = getOutputDataTypeImpl(~)
            dt1 = 'uint8';
            dt2 = 'uint8';
            dt3 = 'uint8';
            dt4 = 'logical';
        end
        function [cp1, cp2, cp3, cp4] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
            cp4 = false;
        end
    end
end
