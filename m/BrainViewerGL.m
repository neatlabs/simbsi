classdef BrainViewerGL < matlab.System &...
        matlab.system.mixin.Propagates
    % BrainViewer Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.
    
    properties(Access = private)
        hm
        indz;
        autoscale = true;
        ny;
        W;
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            obj.hm = evalin('base','hm');
            obj.indz = obj.hm.scalp.vertices(:,3) < min(obj.hm.channelSpace(:,3)) - 0.1*abs(min(obj.hm.channelSpace(:,3)));
            obj.ny = size(obj.hm.scalp.vertices,1);
            obj.W = geometricTools.localGaussianInterpolator(obj.hm.channelSpace,obj.hm.scalp.vertices,0.03,true);
        end
        
        function Data = stepImpl(obj, y, x)
            if nargin < 3
                x = [];
            end
            try %#ok
                % F = scatteredInterpolant(obj.hm.channelSpace,y','natural','linear');
                % y = F(obj.hm.scalp.vertices);
                y = obj.W*y';
                y(obj.indz) = 0;
                if obj.autoscale
                    y = y/(max(abs(y))+eps);
                end
                if obj.autoscale
                    x = x/(max(abs(x))+eps);
                end
                Data = [y(:);x(:)];
            end
        end
        
        function sz = getOutputSizeImpl(obj)
            if isempty(propagatedInputSize(obj, 2))
                sz = [];
            else
                if isempty(obj.hm)
                    obj.hm = evalin('base','hm');
                    obj.ny = size(obj.hm.scalp.vertices,1);
                end
                nx = propagatedInputSize(obj, 2);
                sz = nx(2)+obj.ny;
            end
        end
        function fz = isOutputFixedSizeImpl(~)
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(obj)
            cp = propagatedInputComplexity(obj, 1);
        end
    end
end
