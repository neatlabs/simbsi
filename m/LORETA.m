classdef LORETA < matlab.System  & matlab.system.mixin.Propagates
    % LORETA Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        H;
        L;
        indCortex;
        Nx;
    end

    % Pre-computed constants
    properties(Access = private)
        U;
        s
        s2;
        V;
        m;
        T
    end

    methods(Access = protected)
        function setupImpl(obj)
            obj.V = zeros([size(obj.H,1) obj.Nx]);
            [obj.U,S,Vt] = svd(obj.H/obj.L,'econ');
            obj.V(:,obj.indCortex) = (obj.L\Vt)';
            obj.s = diag(S);
            obj.s2 = obj.s.^2;
            obj.m = [1e-3;1e-3];
            obj.update(obj.m);
        end

        function x = stepImpl(obj, y, M)
            if any(obj.m ~= M)
                obj.m = M;
                obj.update(M);
            end
            x = y*obj.T;
        end
        
        function update(obj, M)
            lambda = M(1);
            gamma_F = M(2);
            psi = gamma_F*obj.s2 + lambda;
            obj.T = gamma_F*obj.U*diag(obj.s./psi)*obj.V;
            %obj.T = gamma_F*obj.U*bsxfun(@times,obj.V,obj.s./psi);
        end
            
        function sz = getOutputSizeImpl(obj)
            sz = propagatedInputSize(obj, 1);
            if ~isempty(sz)
                sz = [sz(1) obj.Nx];
            end
        end
        function fz = isOutputFixedSizeImpl(~)
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(~)
            cp = false;
        end
    end
end
