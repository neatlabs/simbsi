function EEG = sim2EEG(logsout, setname, channelLabels, channelPositions)
if nargin < 2
    setname = 'SimBCI';
end
EEG = eeg_emptyset;
EEG.setname = setname;
indEEG = [];
for k=1:logsout.numElements
    if strcmpi(logsout{k}.Name,'eeg')
        EEG.times = logsout{k}.Values.Time*1000;
        [EEG.nbchan, EEG.pnts] = size(squeeze(logsout{k}.Values.Data));
        EEG.data = squeeze(logsout{k}.Values.Data);
        EEG.srate = round(median(1./diff(logsout{k}.Values.Time)));
        indEEG = k;
    end
end
if isempty(indEEG)
    disp('Did not find EEG data exported from Simulink. Check your model and label the the wire containing EEG data as ''EEG''.');
    return;
end
EEG.xmin  = EEG.times(1);
EEG.xmax  = EEG.times(end);
EEG.trials = 1;
logsout = logsout.removeElement(indEEG);

% Add events
for k=1:logsout.numElements
    if strcmpi(logsout{k}.Name,'events') || strcmpi(logsout{k}.Name,'event')
        indnz = find(logsout{k}.Values.Data);
        n = length(indnz);
        if n
            EEG.event = repmat(struct('latency',0,'type',''),n,1);
            for i=1:n
                EEG.event(i).latency = indnz(i);
                EEG.event(i).type = num2str(logsout{k}.Values.Data(indnz(i)));
                EEG.event(i).urevent = i;
            end
        end
    end
end

% Add channel information (if any)
if nargin < 3, return;end
if nargin < 4
    if EEG.nbchan ~= length(channelLabels)
        disp('EEG.nbchan is diffrent than length(channelLabels). You got to fix this to be able to add channel locations.')
        return;
    end
    hmfile = which('head_modelColin27_5003_Standard-10-5-Cap339.mat');
    if ~exist(hmfile,'file')
        disp('Cannot look up the channel positions. Try adding the headModel (https://github.com/aojeda/headModel) toolbox to the path.');
        return;
    end
    hm = headModel.loadFromFile(hmfile);
    [~,loc1,loc2] = intersect(hm.labels, channelLabels, 'stable');
    if isempty(loc1)
        disp('The labels you entered are not in the head model template. Please make sure to label your channels following the 10-20 system.');
        return;
    end
    channelPositions = nan(EEG.nbchan,3);
    channelPositions(loc2,:) = hm.channelSpace(loc1,:);
end

if EEG.nbchan ~= length(channelPositions)
    disp('EEG.nbchan is diffrent than length(channelPositions). You got to fix this to be able to add channel locations.')
    return;
end
EEG.chanlocs = repmat(struct('labels',[],'type',[],'X',[],'Y',[],'Z',[],'radius',[],'theta',[]),EEG.nbchan,1);
for k=1:EEG.nbchan
    EEG.chanlocs(k).X = channelPositions(k,1);
    EEG.chanlocs(k).Y = channelPositions(k,2);
    EEG.chanlocs(k).Z = channelPositions(k,3);
    if ~isnan(channelPositions(k,1))
        [EEG.chanlocs(k).theta, EEG.chanlocs(k).radius] = cart2pol(channelPositions(k,1), channelPositions(k,2), channelPositions(k,3));
        EEG.chanlocs(k).theta = -EEG.chanlocs(k).theta*180/pi;
    end
    EEG.chanlocs(k).type = 'EEG';
    EEG.chanlocs(k).labels = channelLabels{k};
end
EEG = eeg_checkset(EEG);