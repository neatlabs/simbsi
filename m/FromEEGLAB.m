classdef FromEEGLAB < matlab.System & matlab.system.mixin.Propagates
    % FromEEGLAB Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties(Nontunable)
        filename = '';
    end

    % Pre-computed constants
    properties(Access = private)
        pointer
        srate
        nbchan
        data
        n
        mrk
        clk = zeros(2,6);
    end

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
            EEG = pop_loadset(obj.filename);
            obj.srate = EEG.srate;
            obj.data = double(EEG.data(:,:)');
            obj.nbchan = EEG.nbchan;
            obj.n = size(obj.data,1);
            obj.mrk = zeros(obj.n,1);
            if ~isempty(EEG.event)
                uniqueEvents = unique({EEG.event.type});
                latency = round(cell2mat({EEG.event.latency}));
                for ev=1:length(uniqueEvents)
                    obj.mrk(latency(ismember({EEG.event.type},uniqueEvents{ev}))) = ev;
                end
            end
            obj.pointer = 1;
            obj.clk(2,:) = clock; 
        end

        function [Data,u] = stepImpl(obj)
            obj.pointer(obj.pointer>obj.n) = 1;
            Data = obj.data(obj.pointer,:);
            u = obj.mrk(obj.pointer);
            obj.pointer = obj.pointer+1;
            obj.clk(1,:) = obj.clk(2,:);
            obj.clk(2,:) = clock;
            while etime(obj.clk(2,:),obj.clk(1,:)) < 1/obj.srate
                obj.clk(2,:) = clock;
            end
        end

        function [sz1,sz2] = getOutputSizeImpl(obj)
            if isempty(obj.nbchan) && ~isempty(obj.filename)
                EEG = pop_loadset(obj.filename);
                sz1 = [1 EEG.nbchan];
            else
                sz1 = [1 obj.nbchan];
            end
            sz2 = 1;
        end
        function [fz1, fz2] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
        end
        function [dt1,dt2] = getOutputDataTypeImpl(obj)
            dt1 = 'double';
            dt2 = 'double';
        end
        function [cp1,cp2] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
        end
    end
end
