classdef NoiseAdaptation < matlab.System & matlab.system.mixin.Propagates
    % NoiseAdaptation Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    % Public, tunable properties
    properties
        maxIterations = 50; % Maximum number of iterations
        tolerance = 1e-1;   % Convergence criteria
    end
    
    properties(Nontunable)
        H;
        L;
    end
    
    properties(DiscreteState)
    end

    % Pre-computed constants
    properties(Access = private)
        Ut;
        s
        s2;
        V;
        Ny=0;
        loss;
        m;
        mBufferLength = 20;
        mMin = [1e-3;1e-3];
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            [U,S,Vt] = svd(obj.H/obj.L,'econ');
            obj.Ut = U';
            obj.V = obj.L\Vt;
            obj.s = diag(S);
            obj.s2 = obj.s.^2;
            obj.Ny = size(obj.H,1);
            obj.loss = zeros(obj.maxIterations,1);
            obj.m = obj.mMin*ones(1,obj.mBufferLength);
        end
        
        function [M_F, L] = stepImpl(obj,Y)
            if ~all(Y(:)==0)
                Y = Y';
                UtY = obj.Ut*Y;
                y2 = UtY.^2;
                S = [obj.s2 ones(obj.Ny,1)];
                phi = abs(mean((S'*S)\S'*y2,2));
                lambda = phi(2);
                gamma_F = phi(1);
                gamma_F(gamma_F < 1e-3) = 1e-3;
                obj.loss = obj.loss*0;
                k=1;
                for k=1:obj.maxIterations
                    psi = gamma_F*obj.s2 + lambda;
                    psi2 = psi.^2;
                    lambda   = lambda*sum(mean(bsxfun(@times,y2, 1./psi2),2))/(eps+sum(1./psi));
                    gamma_F  = gamma_F*sum(mean(bsxfun(@times,y2,obj.s2./psi2),2))/(eps+sum(obj.s2./psi));
                    obj.loss(k) = sum(log(psi))+sum(mean(bsxfun(@rdivide,y2,psi),2));
                    if k> 1 && abs(diff(fliplr(obj.loss(k-1:k)))) < obj.tolerance
                        break;
                    end
                end
                obj.m = circshift(obj.m, -1,2);
                obj.m(:,end) = [lambda;gamma_F];
                L = obj.loss(k);
            else
                L=0;
            end
            M_F = mean(obj.m,2);
        end

        function resetImpl(~)
            % Initialize / reset discrete-state properties
        end
        
        function [sz1, sz2] = getOutputSizeImpl(~)
            sz1 = [2 1];
            sz2 = 1;
        end
        function [fz1,fz2] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
        end
        function [dt1,dt2] = getOutputDataTypeImpl(obj)
            dt1 = propagatedInputDataType(obj, 1);
            dt2 = propagatedInputDataType(obj, 1);
        end
        function [cp1,cp2] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
        end
        
    end
end
