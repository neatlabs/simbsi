classdef CortexViewerGL < matlab.System &...
        matlab.system.mixin.Propagates
    % CortexViewerGL Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.
    
    properties(Access = private)
        autoscale = true;
    end
    
    methods(Access = protected)
        
        function x = stepImpl(obj, x)
            if obj.autoscale
                x = x/(max(abs(x))+eps);
            end
            x = x(:);
        end
        
        function sz = getOutputSizeImpl(obj)
            sz = propagatedInputSize(obj, 1);
        end
        function fz = isOutputFixedSizeImpl(~)
            fz = true;
        end
        function dt = getOutputDataTypeImpl(obj)
            dt = propagatedInputDataType(obj, 1);
        end
        function cp = isOutputComplexImpl(obj)
            cp = propagatedInputComplexity(obj, 1);
        end
    end
end
