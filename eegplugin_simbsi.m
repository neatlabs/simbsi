% eegplugin_simbsi() - Plugin for designing online processing pipelines and BCI applications using the Simulink Brain Signal Interface (SimBSI) library.
% Usage:
%   >> eegplugin_simbsi(fig, trystrs, catchstrs);
%
% Inputs:
%   fig        - [integer] eeglab figure.
%   trystrs    - [struct] "try" strings for menu callbacks.
%   catchstrs  - [struct] "catch" strings for menu callbacks.
%
% Author: Alejandro Ojeda, NEATLabs, UCSD, 2018
%
% See also: eeglab()

function vers = eegplugin_simbsi(fig,try_strings, catch_strings)
vers = 'SimBSI1.0.1';
if ~exist('SimBSIui','class')
    libPath = fileparts(which('eeglab'));
    addpath(fullfile(libPath, 'plugins', 'simbsi'));
else
    disp(['Using SimBSI installed in: ' fileparts(which('SimBSIui'))])
end
evalin('base','simbsi = SimBSIui();');

h = uimenu( fig, 'Label', 'SimBSI');
uimenu( h,       'Label', 'Simulate headset','Callback','[ALLEEG,EEG,CURRENTSET] = pop_newSimBSIEEG(ALLEEG);');
uimenu( h,       'Label', 'Examples',        'Callback','simbsi.showGui();');

