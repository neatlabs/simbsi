classdef SimBSIui < handle
    
    properties
        dependency = {...
            'Simulink',...
            'DSP System Toolbox',...
            'Stateflow',...
            'GUI Layout Toolbox',...
            'Simulink Support Package for Raspberry Pi Hardware',...
            'MATLAB Compiler SDK',...
            };
        optionalEEGLABDependency = {'firfilt','dsi','xdfimport'}
    end
    
    properties(Dependent)
        rootDir
    end
    
    methods
        
        % Constructor
        function obj = SimBSIui(show)
            if nargin < 1, show = false;end
            libPath = obj.rootDir;
            addpath(genpath(fullfile(libPath,'m')));
            addpath(genpath(fullfile(libPath,'resources')));
            
            % Adding plugins
            items = dir(fullfile(obj.rootDir,'plugins'));
            rmthis = ismember({items.name},{'.','..'});
            items(rmthis) = [];
            items = items(cell2mat({items.isdir}));
            for k=1:length(items)
                addpath(genpath(fullfile(libPath,'plugins',items(k).name)));
                disp(['Adding SimBSI plugin ' items(k).name]);
            end
            obj.install();
            if show
                obj.showGui();
            end
        end
        
        function showGui(obj)
            fig = findall(0,'tag','SimBSI Examples');
            if ~isempty(fig)
                return
            end
            iconNew = imresize(imread(fullfile(obj.rootDir,'resources','icons','Gnome-document-new.svg.png')),[28 28]);
            iconOpen = imresize(imread(fullfile(obj.rootDir,'resources','icons','Gnome-document-open.svg.png')),[28 28]);
            iconHelp = imresize(imread(fullfile(obj.rootDir,'resources','icons','Gnome-help-browser.svg.png')),[28 28]);
            imgEEGLAB = imread(fullfile(obj.rootDir,'resources','EEGLAB_Simulink.png'));
            imgLSL = imread(fullfile(obj.rootDir,'resources','LSL_Simulink.png'));
            imgStateflow = imread(fullfile(obj.rootDir,'resources','Stateflow_example.png'));
            imgAlphaPower = imread(fullfile(obj.rootDir,'resources','Alpha_Power.png'));
            
            fig = figure('Menu','none','Toolbar','None','NumberTitle','off','Tag','SimBSI Examples','Name','SimBSI Examples');
            fig.Position(3:4) = [657   255];
            tb = uitoolbar(fig);
            uipushtool(tb,'Separator','on', 'CData',iconNew,'TooltipString','Create a new pipeline','ClickedCallback',@newPipeline);
            uipushtool(tb,'Separator','off', 'CData',iconOpen,'TooltipString','Open Simulink library','ClickedCallback',@openLib);
            uipushtool(tb,'Separator','off','CData',iconHelp,'TooltipString','Help','ClickedCallback','web(''https://bitbucket.org/neatlabs/simbsi/wiki/Home'',''-browser'')');
            tabgp = uitabgroup(fig);
            tab1 = uitab(tabgp,'Title','Example page 1');
            btn1 = uicontrol( 'Parent', tab1, 'style', 'pushbutton', 'String','', 'CData',imgEEGLAB,'Callback',@open_EEGLAB_Simulink,'Units','Normalized');
            btn2 = uicontrol( 'Parent', tab1, 'style', 'pushbutton', 'String','', 'CData',imgLSL,'Callback',@open_EEG_PipelineLSL,'Units','Normalized');
            btn1.Position = [0 0 0.5 1];
            btn2.Position = [0.5 0 0.5 1];
            btn1.BackgroundColor = [0.93 0.96 1];
            btn2.BackgroundColor = [0.93 0.96 1];
            btn1.TooltipString = 'Click here to launch EEGLAB Simulink interface';
            btn2.TooltipString = 'Click here to launch the online prcessing pipeline';
            
            tab2 = uitab(tabgp,'Title','Example page 2');
            btn3 = uicontrol( 'Parent', tab2, 'style', 'pushbutton', 'String','', 'CData',imgStateflow,'Callback',@open_state_flow,'Units','Normalized');
            btn4 = uicontrol( 'Parent', tab2, 'style', 'pushbutton', 'String','', 'CData',imgAlphaPower,'Callback',@open_alpha_power,'Units','Normalized');
            btn3.Position = [0 0 0.5 1];
            btn4.Position = [0.5 0 0.5 1];
            btn3.BackgroundColor = [0.93 0.96 1];
            btn4.BackgroundColor = [0.93 0.96 1];
            btn3.TooltipString = 'Click here to launch the Flanker task stateflow example';
            btn4.TooltipString = 'Click here to track the peak of the alpha rhythm online';
        end
        
        % Utitlity for retrieving SimBSI directory
        function val = get.rootDir(obj)
            val = fileparts(which('SimBSIui'));
        end
        
        function flag = isInstalled(obj)
            flag = exist(fullfile(obj.rootDir,'isInstalled.txt'),'file');
        end
        
        % Install dependency and compile mex files
        function install(obj)
            if obj.isInstalled
                return
            end
            
            % Check add-ons
            fprintf('Checking MATLAB dependencies...');
            addons = matlab.addons.installedAddons;
            [~,~,ind] = intersect(addons.Name,obj.dependency, 'stable');
            missing = setdiff(obj.dependency,obj.dependency(ind));
            isMissing = ~isempty(missing);
            if isMissing
                s = sprintf('The following MATLAB add-ons are missing:');
                for i=1:length(missing)
                    s = sprintf('%s\n\t%s',s,missing{i});
                end
                warning('%s\nIf you ignore this warning some components of the SimBSI library may not work properly.',s)
                disp('You can install the missing toolboxes with <a href="https://www.mathworks.com/help/matlab/matlab_env/get-add-ons.html">Get Add-Ons</a>.');
            else
                fprintf('ok\n');
            end
            pause(0.5);
            
            % Check EEGLAB
            eeglabDir = fileparts(which('eeglab'));
            fprintf('Checking EEGLAB (optional) dependencies...');
            if isempty(eeglabDir)
                disp('<a href="https://github.com/sccn/eeglab">EEGLAB</a> is missing, some SimBSI modules for EEG processing will not work.')
            else
                eeglabPlugins = dir(fullfile(eeglabDir,'plugins'));
                eeglabPlugins = {eeglabPlugins.name};
                ind = contains(eeglabPlugins,obj.optionalEEGLABDependency);
                missing = obj.optionalEEGLABDependency(~contains(eeglabPlugins(ind),obj.optionalEEGLABDependency));
                if ~isempty(missing)
                    s = sprintf('The following EEGLAB plugins are missing:');
                    for i=1:length(missing)
                        s = sprintf('%s\n\t%s',s,missing{i});
                    end
                    warning('%s\nIf you ignore this warning some components of the SimBSI library may not work properly.',s);
                    disp('To install the missing toolboxes go to <a href="https://bitbucket.org/neatlabs/simbsi/wiki/Installation">Install SimBSI</a>.');
                else
                    fprintf('ok\n');
                end
            end
            pause(0.5);
            
            % Save current working directory
            wdir = pwd;
            
            % Move to SimBSI directory
            cd(obj.rootDir);
            warning off;
            
            % Compile mex files
            try
                disp('Compiling mex files...');
                pause(0.5);
                
                if isunix
                    mex -v -g c/ReadFromBinaryFile.c
                    pause(0.5);
                    mex -v -g c/LSLInlet.c c/liblsl.so
                    pause(0.5);
                    mex -v -g c/LSLOutlet.c c/liblsl.so
                    pause(0.5);
                    mex -v -g c/ListLSLStreams.c c/liblsl.so
                    system(['sudo cp c/liblsl.so ' fullfile(matlabroot,'sys','os','glnxa64')])
                    
                elseif ispc
                    disp('Checking for compiler');
                    if ~any(ismember(addons.Name,'MATLAB Support for MinGW-w64 C/C++ Compiler'))
                        disp('SimBSI mex functions have been compiled and tested on Windows using the MinGW-w64 C/C++ Compiler, however, other (untested) compilers such as MS Visual C may work as well.');
                        disp('You can install MinGW-w64 C/C++ Compiler with <a href="https://www.mathworks.com/help/matlab/matlab_env/get-add-ons.html">Get Add-Ons</a>.');
                    end
                    pause(0.5);
                    mex -v -g c/LSLInlet.c c/liblsl64.lib
                    pause(0.5);
                    mex -v -g c/LSLOutlet.c c/liblsl64.lib
                    pause(0.5);
                    mex -v -g c/ListLSLStreams.c c/liblsl64.lib
                    pause(0.5);
                    mex -v -g c/ReadFromBinaryFile.c c/liblsl64.lib
                    copyfile('c/liblsl64.dll','.');
                    
                elseif ismac
                    mex -v -g c/ReadFromBinaryFile.c
                    pause(0.5);
                    mex -v -g c/LSLInlet.c c/liblsl64.dylib
                    pause(0.5);
                    mex -v -g c/LSLOutlet.c c/liblsl64.dylib
                    pause(0.5);
                    mex -v -g c/ListLSLStreams.c c/liblsl64.dylib
                    system(['sudo cp c/liblsl64.dylib ' fullfile(matlabroot,'sys','os','glnxa64')])
                else
                    error('Sorry, we only support Linux, Windows, and Mac.');
                end
                pause(0.5);
                fclose(fopen(fullfile(obj.rootDir,'isInstalled.txt'),'w'));
            catch ME
                disp(ME)
                if exist(fullfile(obj.rootDir,'isInstalled.txt'),'file')
                    delete(fullfile(obj.rootDir,'isInstalled.txt'));
                end
            end
            warning on;
            cd(wdir);
        end
        function disp(~)
            tb = table({'simbsi.help()';'simbsi.newPipeline()';'simbsi.install()';'simbsi.openLibrary'},{'Display SimBSI documentation on a web browser';'Create a new Simulink pipeline.';'Check for missing dependencies and compile required mex files.';'Open Simulink Library Browser.'});
            tb.Properties.VariableNames =  {'Command';'Description'};
            fprintf('<strong>SimBSI user interface documentation</strong>\n\n')
            disp(tb);
            fprintf('Visit <a href="https://bitbucket.org/neatlabs/simbsi/wiki/Home">SimBSI documentation</a> for mere details.\n')
        end
    end
    methods(Static)
        
        function openLibrary()
            slLibraryBrowser('open');
        end
        
        function newPipeline(pipelineName, stepTime)
            if nargin < 2
                in = inputdlg({'Pipeline name', 'Step time (sec)'},'',1,{'',''});
                if isempty(in)
                    return;
                end
                pipelineName = in{1};
                stepTime = in{2};
            end
            sys = new_system(pipelineName);
            cs = getActiveConfigSet(sys);
            cs.set_param('StartTime','0');
            cs.set_param('StopTime','inf');
            cs.set_param('SolverType','fixed-step');
            cs.set_param('FixedStep',stepTime);
            cs.set_param('DSMLogging','off');
            cs.set_param('SignalLogging','off');
            cs.set_param('SaveOutput','off');
            cs.set_param('SaveState','off');
            cs.set_param('SaveTime','off');
            open_system(sys);
        end
        function help()
            web('https://bitbucket.org/neatlabs/simbsi/wiki/Home','-browser');
        end
    end
end

%%
function newPipeline(src,evnt)
SimBSIui.newPipeline();
end

function openLib(src,evnt)
src.Parent.Parent.Pointer = 'watch';
drawnow
slLibraryBrowser;
src.Parent.Parent.Pointer = 'arrow';
end

function open_EEGLAB_Simulink(src,evnt)
src.Parent.Parent.Parent.Pointer = 'watch';
drawnow
rootDir = fileparts(which('SimBSIui.m'));
open(fullfile(rootDir,'examples','EEGLAB_Simulink.slx'));
src.Parent.Parent.Parent.Pointer = 'arrow';
end

function open_EEG_PipelineLSL(src,evnt)
src.Parent.Parent.Parent.Pointer = 'watch';
drawnow
rootDir = fileparts(which('SimBSIui.m'));
open(fullfile(rootDir,'examples','EEGPipeline_LSL.slx'));
src.Parent.Parent.Parent.Pointer = 'arrow';
end

function open_state_flow(src,evnt)
src.Parent.Parent.Parent.Pointer = 'watch';
drawnow
rootDir = fileparts(which('SimBSIui.m'));
open(fullfile(rootDir,'examples','FlankerTask.slx'));
src.Parent.Parent.Parent.Pointer = 'arrow';
end

function open_alpha_power(src,evnt)
src.Parent.Parent.Parent.Pointer = 'watch';
drawnow
rootDir = fileparts(which('SimBSIui.m'));
open(fullfile(rootDir,'examples','Alpha_Power_Pipeline.slx'));
src.Parent.Parent.Parent.Pointer = 'arrow';
end


